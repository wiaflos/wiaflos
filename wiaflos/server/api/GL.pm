# SOAP interface to GL module
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2006-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::GL;

use strict;
use warnings;

use wiaflos::server::api::auth;
use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::core::GL;


# Plugin info
our $pluginInfo = {
	Name => "General Ledger",
	Init => \&init,
};


# Initialize module
sub init {
	my $server = shift;


	# Ledger
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/GL','getGLAccounts','GL/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/GL','getGLAccountEntries','GL/Show');

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/GL','createGLAccount','GL/Add');

	# Transactions
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/GL','getGLTransactions','GL/Transaction/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/GL','getGLTransactionEntries','GL/Transaction/Show');

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/GL','createGLTransaction','GL/Transaction/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/GL','linkGLTransaction','GL/Transaction/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/GL','postGLTransaction','GL/Transaction/Post');

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/GL','removeGLTransaction','GL/Transaction/Remove');
}





# Return list of GL accounts
sub getGLAccounts {
	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $rawData = wiaflos::server::core::GL::getGLAccounts();
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::GL::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'ParentAccountID'} = $item->{'ParentAccountID'};
		$tmpItem->{'Name'} = $item->{'Name'};
		$tmpItem->{'Number'} = $item->{'Number'};
		$tmpItem->{'FinCatCode'} = $item->{'FinCatCode'};
		$tmpItem->{'FinCatDescription'} = $item->{'FinCatDescription'};
		$tmpItem->{'RwCatCode'} = $item->{'RwCatCode'};
		$tmpItem->{'RwCatDescription'} = $item->{'RwCatDescription'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Return list of GL transactions
sub getGLTransactions {
	my (undef,$data) = @_;


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Type'} = $data->{'Type'};
	$detail->{'StartDate'} = $data->{'StartDate'};
	$detail->{'EndDate'} = $data->{'EndDate'};
	# Grab and sanitize data
	my $rawData = wiaflos::server::core::GL::getGLTransactions($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::GL::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'TransactionDate'} = $item->{'TransactionDate'};
		$tmpItem->{'Reference'} = $item->{'Reference'};
		$tmpItem->{'Type'} = $item->{'Type'};
		$tmpItem->{'Posted'} = $item->{'Posted'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Create GL transaction
# Parameters:
#		Date 	- Transaction date
#		Reference		- Transactoin reference
sub createGLTransaction {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Date'}) || $data->{'Date'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Date' invalid");
	}

	if (!defined($data->{'Reference'}) || $data->{'Reference'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Reference' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Date'} = $data->{'Date'};
	$detail->{'Reference'} = $data->{'Reference'};
	$detail->{'Type'} = $data->{'Type'};
	my $res = wiaflos::server::core::GL::createGLTransaction($detail);

	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::GL::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Link GL transaction
# Parameters:
#		ID				- Transaction ID
#		GLAccountNumber	- GL account number
#		Amount			- Amount to post
#		Reference			- Entry reference
sub linkGLTransaction {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'ID'}) || $data->{'ID'} < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ID' invalid");
	}

	if (!defined($data->{'GLAccountNumber'}) || $data->{'GLAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLAccountNumber' invalid");
	}

	if (!defined($data->{'Amount'}) && !defined($data->{'Credit'}) && !defined($data->{'Debit'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Amount' or 'Credit' or 'Debit' must be specified");
	}
	if (defined($data->{'Credit'}) && defined($data->{'Debit'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Credit' and 'Debit' cannot be specified for the same transcation");
	}
	if ((defined($data->{'Credit'}) && defined($data->{'Amount'})) || (defined($data->{'Debit'}) && defined($data->{'Amount'}))) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Credit'/'Debit' is incompatible with 'Amount'");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'ID'} = $data->{'ID'};
	$detail->{'GLAccountNumber'} = $data->{'GLAccountNumber'};
	$detail->{'Amount'} = $data->{'Amount'};
	$detail->{'Credit'} = $data->{'Credit'};
	$detail->{'Debit'} = $data->{'Debit'};
	$detail->{'Reference'} = $data->{'Reference'};
	my $res = wiaflos::server::core::GL::linkGLTransactionByAccountNumber($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::GL::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Post GL transaction
# Parameters:
#		ID		- Transaction ID
sub postGLTransaction {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'ID'}) || $data->{'ID'} < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ID' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'ID'} = $data->{'ID'};
	my $res = wiaflos::server::core::GL::postGLTransaction($detail);

	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::GL::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Remove GL transaction
# Parameters:
#		ID	- Transaction ID
sub removeGLTransaction {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'ID'}) || $data->{'ID'} < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ID' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'ID'} = $data->{'ID'};
	my $res = wiaflos::server::core::GL::removeGLTransaction($detail);

	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::GL::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Return list of GL transaction entries
# Parameters:
#		ID	- Transaction ID
sub getGLTransactionEntries {
	my (undef,$data) = @_;


	if (!defined($data->{'ID'}) || $data->{'ID'} < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ID' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'ID'} = $data->{'ID'};
	my $rawData = wiaflos::server::core::GL::getGLTransactionEntries($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::GL::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'TransactionID'} = $item->{'TransactionID'};
		$tmpItem->{'TransactionReference'} = $item->{'TransactionReference'};
		$tmpItem->{'GLAccountID'} = $item->{'GLAccountID'};
		$tmpItem->{'GLAccountName'} = $item->{'GLAccountName'};
		$tmpItem->{'GLAccountNumber'} = $item->{'GLAccountNumber'};
		$tmpItem->{'Amount'} = $item->{'Amount'};
		$tmpItem->{'Reference'} = $item->{'Reference'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


## @fn getGLAccountEntries($data)
# Return list of GL account entries
#
# @param data Parameter hash ref
# @li AccountNumber Supplier code
# @li StartDate Start date
# @li EndDate End date
# @li BalanceBroughtForward Flag to indicate if we must generate a BBF
#
# @returns Array ref of hash refs
# @li ID ID of GL account entry
# @li TransactionID Transaction ID
# @li Reference GL account entry reference
# @li TransactionDate Transaction date
# @li Amount Amount
# @li TransactionReference Transaction reference
sub getGLAccountEntries {
	my (undef,$data) = @_;


	if (!defined($data->{'AccountNumber'}) || $data->{'AccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'AccountNumber' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'AccountNumber'} = $data->{'AccountNumber'};
	$detail->{'StartDate'} = $data->{'StartDate'};
	$detail->{'EndDate'} = $data->{'EndDate'};
	$detail->{'Type'} = $data->{'Type'};
	$detail->{'BalanceBroughtForward'} = $data->{'BalanceBroughtForward'};
	my $rawData = wiaflos::server::core::GL::getGLAccountEntries($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::GL::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'TransactionID'} = $item->{'TransactionID'};
		$tmpItem->{'Reference'} = $item->{'Reference'};
		$tmpItem->{'TransactionDate'} = $item->{'TransactionDate'};
		$tmpItem->{'Amount'} = $item->{'Amount'};
		$tmpItem->{'TransactionReference'} = $item->{'TransactionReference'};
		$tmpItem->{'TransactionType'} = $item->{'TransactionType'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Create GL account
# Parameters:
#		Code		- Account code
#		Name		- Account name
#		FinCatCode	- Financial category
#		RwCatCode	- Reporting category
# Optional:
#		ParentAccountNumber	- Parent GL account
sub createGLAccount {
	my (undef,$data) = @_;


	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	if (!defined($data->{'Name'}) || $data->{'Name'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Name' invalid");
	}

	if (!defined($data->{'FinCatCode'}) || $data->{'FinCatCode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'FinCatCode' invalid");
	}

	if (!defined($data->{'RwCatCode'}) || $data->{'RwCatCode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'RwCatCode' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'Name'} = $data->{'Name'};
	$detail->{'FinCatCode'} = $data->{'FinCatCode'};
	$detail->{'RwCatCode'} = $data->{'RwCatCode'};
	$detail->{'ParentAccountNumber'} = $data->{'ParentAccountNumber'};
	my $res = wiaflos::server::core::GL::createGLAccount($detail);

	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::GL::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



1;
# vim: ts=4
