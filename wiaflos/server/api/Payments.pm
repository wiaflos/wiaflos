# SOAP interface to Payments module
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::Payments;

use strict;
use warnings;

use wiaflos::server::api::auth;
use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::core::Payments;

# Plugin info
our $pluginInfo = {
	Name => "Payments",
	Init => \&init,
};


# Initialize module
sub init {
	my $server = shift;


	# Payments
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Payments','getPayments','Payments/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Payments','getPayment','Payments/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Payments','createPayment','Payments/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Payments','postPayment','Payments/Post');
	# Allocations
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Payments','getPaymentAllocations','Payments/Allocation/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Payments','createPaymentAllocation','Payments/Allocation/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Payments','postPaymentAllocation','Payments/Allocation/Post');
}



# Create payment
# Parameters:
#		SupplierCode	- Supplier code
#		GLAccountNumber	- GL account where money was paid from
#		Number	- Reference for this payment
#		Date		- Date of payment
#		Reference			- GL account entry reference (bank statement reference for example)
#		Amount		- Amount
sub createPayment {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'SupplierCode'}) || $data->{'SupplierCode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'SupplierCode' invalid");
	}

	if (!defined($data->{'GLAccountNumber'}) || $data->{'GLAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLAccountNumber' invalid");
	}

	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'Date'}) || $data->{'Date'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Date' invalid");
	}

	if (!defined($data->{'Reference'}) || $data->{'Reference'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Reference' invalid");
	}

	if (!defined($data->{'Amount'}) || $data->{'Amount'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Amount' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'SupplierCode'} = $data->{'SupplierCode'};
	$detail->{'GLAccountNumber'} = $data->{'GLAccountNumber'};
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'Date'} = $data->{'Date'};
	$detail->{'Reference'} = $data->{'Reference'};
	$detail->{'Amount'} = $data->{'Amount'};
	my $res = wiaflos::server::core::Payments::createPayment($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Payments::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Sanitize payment
sub sanitizePayment {
	my $params = shift;

	my $tmpItem;

	$tmpItem->{'ID'} = $params->{'ID'};
	$tmpItem->{'SupplierID'} = $params->{'SupplierID'};
	$tmpItem->{'SupplierCode'} = $params->{'SupplierCode'};

	$tmpItem->{'GLAccountID'} = $params->{'GLAccountID'};
	$tmpItem->{'GLAccountNumber'} = $params->{'GLAccountNumber'};

	$tmpItem->{'Number'} = $params->{'Number'};

	$tmpItem->{'TransactionDate'} = $params->{'TransactionDate'};
	$tmpItem->{'Reference'} = $params->{'Reference'};
	$tmpItem->{'Amount'} = $params->{'Amount'};

	$tmpItem->{'GLTransactionID'} = $params->{'GLTransactionID'};
	$tmpItem->{'Posted'} = $params->{'Posted'};

	$tmpItem->{'Closed'} = $params->{'Closed'};

	return $tmpItem;
}


# Sanitize raw payment allocation
sub sanitizePaymentAllocation
{
	my $param = shift;


	my $tmpItem;

	$tmpItem->{'ID'} = $param->{'ID'};

	$tmpItem->{'Number'} = $param->{'Number'};

	$tmpItem->{'SupplierInvoiceID'} = $param->{'SupplierInvoiceID'};
	$tmpItem->{'SupplierInvoiceNumber'} = $param->{'SupplierInvoiceNumber'};

	$tmpItem->{'Amount'} = $param->{'Amount'};

	$tmpItem->{'GLTransactionID'} = $param->{'GLTransactionID'};
	$tmpItem->{'Posted'} = $param->{'Posted'};

	return $tmpItem;
}


# Return list of payments
sub getPayments {
	my (undef,$data) = @_;


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Type'} = $data->{'Type'};
	my $rawData = wiaflos::server::core::Payments::getPayments($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Payments::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		push(@data,sanitizePayment($item));
	}

	return SOAPResponse(RES_OK,\@data);
}


# Return a payment
sub getPayment {
	my (undef,$data) = @_;


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'PaymentID'} = $data->{'PaymentID'};
	my $rawData = wiaflos::server::core::Payments::getPayment($detail);
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Payments::Error());
	}

	return SOAPResponse(RES_OK,sanitizePayment($rawData));
}


# Post payment
# Parameters:
#		Number	- Reference for this payment
sub postPayment
{
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	my $res = wiaflos::server::core::Payments::postPayment($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Payments::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Post allocation
# Parameters:
#		ID		- Allocation ID to post
sub postPaymentAllocation
{
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'ID'}) || $data->{'ID'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ID' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'ID'} = $data->{'ID'};
	my $res = wiaflos::server::core::Payments::postPaymentAllocation($detail);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Payments::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Create allocation
# Parameters:
#		PaymentNumber	- Payment number
#		SupplierInvoiceNumber	- Supplier invoice number
#		Amount		- Amount
sub createPaymentAllocation {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'PaymentNumber'}) || $data->{'PaymentNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'SupplierInvoiceNumber'}) || $data->{'SupplierInvoiceNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'SupplierInvoiceNumber' invalid");
	}

	if (!defined($data->{'Amount'}) || $data->{'Amount'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Amount' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'PaymentNumber'} = $data->{'PaymentNumber'};
	$detail->{'SupplierInvoiceNumber'} = $data->{'SupplierInvoiceNumber'};
	$detail->{'Amount'} = $data->{'Amount'};
	my $res = wiaflos::server::core::Payments::createPaymentAllocation($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Payments::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Return list of payment allocations
# Parameters:
#		PaymentNumber	- Number for this payment
sub getPaymentAllocations {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'PaymentNumber'}) || $data->{'PaymentNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'PaymentNumber'} = $data->{'PaymentNumber'};
	my $rawData = wiaflos::server::core::Payments::getPaymentAllocations($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Payments::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		push(@data,sanitizePaymentAllocation($item));
	}

	return SOAPResponse(RES_OK,\@data);
}




1;
# vim: ts=4
