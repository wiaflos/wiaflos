# SOAP test functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


# Test functions for soap implementation
package wiaflos::server::api::SOAPTest;

use strict;
use warnings;

use wiaflos::server::api::auth;



# Plugin info
our $pluginInfo = {
	Name => "SOAP Test",
	Init => \&init,
};


# Initialize module
sub init {
	my $server = shift;

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SOAPTest','ping','SOAP/Test');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SOAPTest','paramTest','SOAP/Test');
}


# Return a basic ping
sub ping {
	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	return "Pong reply to user \"" . $authInfo->{'Username'} . "\" - Unix time index ".time().".";
}


# Return a list of params we were given
sub paramTest {
	my ($m,$a,$b,$c) = @_;

	return "Params - Param 1: \"$a\", Param 2: \"$b\", Param 3: \"$c\"";
}




1;
# vim: ts=4
