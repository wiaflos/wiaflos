# SOAP interface to Suppliers module
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::Suppliers;

use strict;
use warnings;

use wiaflos::server::api::auth;
use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::core::Suppliers;


# Plugin info
our $pluginInfo = {
	Name => "Suppliers",
	Init => \&init,
};


# Initialize module
sub init {
	my $server = shift;

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Suppliers','getSuppliers','Suppliers/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Suppliers','getSupplier','Suppliers/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Suppliers','createSupplier','Suppliers/Add');

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Suppliers','removeSupplier','Suppliers/Remove');

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Suppliers','getSupplierGLAccountEntries','Suppliers/GL/Show');

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Suppliers','linkSupplierAddress','Suppliers/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Suppliers','linkSupplierEmailAddress','Suppliers/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Suppliers','linkSupplierPhoneNumber','Suppliers/Add');

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Suppliers','getSupplierAddresses','Suppliers/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Suppliers','getSupplierEmailAddresses','Suppliers/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Suppliers','getSupplierPhoneNumbers','Suppliers/Show');
}



# Return a single supplier
sub getSupplier {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	my $detail;
	$detail->{'Code'} = $data->{'Code'};

	# Grab and sanitize data
	my $rawData = wiaflos::server::core::Suppliers::getSupplier($detail);
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Suppliers::Error());
	}

	# Build result
	my $tmpItem;

	$tmpItem->{'ID'} = $rawData->{'ID'};
	$tmpItem->{'Code'} = $rawData->{'Code'};
	$tmpItem->{'Name'} = $rawData->{'Name'};
	$tmpItem->{'RegNumber'} = $rawData->{'RegNumber'};
	$tmpItem->{'TaxReference'} = $rawData->{'TaxReference'};
	$tmpItem->{'GLAccountID'} = $rawData->{'GLAccountID'};
	$tmpItem->{'GLAccountNumber'} = $rawData->{'GLAccountNumber'};
	$tmpItem->{'ContactPerson'} = $rawData->{'ContactPerson'};


	return SOAPResponse(RES_OK,$tmpItem);
}


# Return list of suppliers
sub getSuppliers {
	my $authInfo = wiaflos::server::api::auth::sessionGetData();


	# Grab and sanitize data
	my $rawData = wiaflos::server::core::Suppliers::getSuppliers();;
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Suppliers::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Code'} = $item->{'Code'};
		$tmpItem->{'Name'} = $item->{'Name'};
		$tmpItem->{'RegNumber'} = $item->{'RegNumber'};
		$tmpItem->{'TaxReference'} = $item->{'TaxReference'};
		$tmpItem->{'GLAccountID'} = $item->{'GLAccountID'};
		$tmpItem->{'GLAccountNumber'} = $item->{'GLAccountNumber'};
		$tmpItem->{'ContactPerson'} = $item->{'ContactPerson'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Create supplier
# Parameters:
#		Code	 - Supplier code
#		Name	- Supplier name
#		GLAccountNumber - General ledger account
# Optional:
#		RegNumber - Company registration number / ID number of person
#		TaxReference - Company tax reference number
#		CreateSubAccount - Create sub GL account for this client
sub createSupplier {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	if (!defined($data->{'Name'}) || $data->{'Name'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Name' invalid");
	}

	if (!defined($data->{'GLAccountNumber'}) || $data->{'GLAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLAccountNumber' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'Name'} = $data->{'Name'};
	$detail->{'GLAccountNumber'} = $data->{'GLAccountNumber'};
	$detail->{'RegNumber'} = $data->{'RegNumber'};
	$detail->{'TaxReference'} = $data->{'TaxReference'};
	$detail->{'CreateSubAccount'} = $data->{'CreateSubAccount'};
	my $res = wiaflos::server::core::Suppliers::createSupplier($detail);

	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Suppliers::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Link address
# Parameters:
#		Code		- Supplier code
#		Type		- Address type, billing or shipping
#		Address		- Address
sub linkSupplierAddress {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	if (!defined($data->{'Type'}) || $data->{'Type'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Type' invalid");
	}

	if (!defined($data->{'Address'}) || $data->{'Address'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Address' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'Type'} = $data->{'Type'};
	$detail->{'Address'} = $data->{'Address'};
	my $res = wiaflos::server::core::Suppliers::linkSupplierAddress($detail);

	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Suppliers::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Link email address
# Parameters:
#		Code		- Supplier code
#		Type		- Address type, accounts or general
#		Address		- Email address
sub linkSupplierEmailAddress {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	if (!defined($data->{'Type'}) || $data->{'Type'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Type' invalid");
	}

	if (!defined($data->{'Address'}) || $data->{'Address'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Address' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'Type'} = $data->{'Type'};
	$detail->{'Address'} = $data->{'Address'};
	my $res = wiaflos::server::core::Suppliers::linkSupplierEmailAddress($detail);

	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Suppliers::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Link phone number
# Parameters:
#		Code		- Supplier code
#		Type		- Address type, billing or shipping
#		Number		- Phone number
# Optional:
#		Name		- Name of person/org
sub linkSupplierPhoneNumber {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	if (!defined($data->{'Type'}) || $data->{'Type'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Type' invalid");
	}

	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'Type'} = $data->{'Type'};
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'Name'} = $data->{'Name'};
	my $res = wiaflos::server::core::Suppliers::linkSupplierPhoneNumber($detail);

	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Suppliers::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Return list of supplier addresses
# Parameters:
#		Code	- Supplier code
sub getSupplierAddresses {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $rawData = wiaflos::server::core::Suppliers::getSupplierAddresses($data);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Suppliers::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Type'} = $item->{'Type'};
		$tmpItem->{'Address'} = $item->{'Address'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Return list of supplier email addresses
# Parameters:
#		Code	- Supplier code
sub getSupplierEmailAddresses {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $rawData = wiaflos::server::core::Suppliers::getSupplierEmailAddresses($data);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Suppliers::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Type'} = $item->{'Type'};
		$tmpItem->{'Address'} = $item->{'Address'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Return list of supplier phone numbers
# Parameters:
#		Code	- Supplier code
sub getSupplierPhoneNumbers {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $rawData = wiaflos::server::core::Suppliers::getSupplierPhoneNumbers($data);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Suppliers::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Type'} = $item->{'Type'};
		$tmpItem->{'Number'} = $item->{'Number'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Remove supplier
# Parameters:
#		Code	- Supplier code
sub removeSupplier {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	my $res = wiaflos::server::core::Suppliers::removeSupplier($detail);

	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Suppliers::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


## @fn getSupplierGLAccountEntries($data)
# Return list of supplier GL account entries
#
# @param data Parameter hash ref
# @li Code Supplier code
# @li StartDate Start date
# @li EndDate End date
#
# @returns Array ref of hash refs
# @li ID ID of GL account entry
# @li TransactionID Transaction ID
# @li Reference GL account entry reference
# @li TransactionDate Transaction date
# @li Amount Amount
# @li TransactionReference Transaction reference
sub getSupplierGLAccountEntries {
	my (undef,$data) = @_;


	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'StartDate'} = $data->{'StartDate'};
	$detail->{'EndDate'} = $data->{'EndDate'};
	$detail->{'BalanceBroughtForward'} = $data->{'BalanceBroughtForward'};
	my $rawData = wiaflos::server::core::Suppliers::getGLAccountEntries($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Suppliers::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'TransactionID'} = $item->{'TransactionID'};
		$tmpItem->{'Reference'} = $item->{'Reference'};
		$tmpItem->{'TransactionDate'} = $item->{'TransactionDate'};
		$tmpItem->{'Amount'} = $item->{'Amount'};
		$tmpItem->{'TransactionReference'} = $item->{'TransactionReference'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}






1;
# vim: ts=4
