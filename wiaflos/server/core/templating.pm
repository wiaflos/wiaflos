# Templating functions
# Copyright (C) 2009-2019, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::templating;

use strict;
use warnings;


use Cwd qw(abs_path);
use File::Basename;
use File::Copy;
use Template;
use Template::Exception;

# Check if we have PDF support using awit-docplates
my $SUPPORTS_PDF = 0;
if (eval {require AWIT::Docplate; require AWIT::PDFLatex; 1;}) {
	$SUPPORTS_PDF = 1;
}

# Exporter stuff
require Exporter;
our (@ISA,@EXPORT,@EXPORT_OK);
@ISA = qw(Exporter);
@EXPORT = qw(
	loadTemplate
	loadTemplate2
);
@EXPORT_OK = qw(
);


# Server instance
my $config;


# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}


# Initialize templating engine
sub Init
{
	my $server = shift;

	$config = $server->{'template_config'};
};



# New version of loadTemplate that uses AWIT Docplates
sub loadTemplate2
{
	my ($templateName,$vars,$outputFile,$format) = @_;


	# Make sure $outputFile is not a reference
	if (ref($outputFile)) {
		setError("CODE ERROR: \$outputFile should never be a ref");
		return;
	}

	# If this is HTML templating return the old loadTemplate() results
	if ($format eq "html") {
		return loadTemplate($templateName,$vars,$outputFile);
	}

	# Make sure this is for pdf...
	if ($format ne "pdf") {
		setError("Unsupported format '$format'");
		return;
	}

	# Make sure this is for pdf...
	if (!$SUPPORTS_PDF) {
		setError("AWIT-Docplates couldn't be loaded");
		return;
	}

	# Create our docplate object
	my $docplate = AWIT::Docplate->new();
	# Loop with each of them
	foreach my $var (keys %{$vars}) {
		# Add variable
		$docplate->addVariable($var,$vars->{$var});
	}
	# Add data dir
	$docplate->addDataPath($config->{'docplates_data'});
	# Work out the TOPDIR and add it
	my $docplatesTOPDIR = dirname($config->{'docplates_data'});
	$docplate->addVariable("TOPDIR",$docplatesTOPDIR);

	# Create temporary dir and files we'll be using
	my $tempDir = File::Temp->newdir();
	my $texFile = sprintf('%s/file.tex',$tempDir);
	my $pdfFile = sprintf('%s/file.pdf',$tempDir);

	# Add the CURDIR, which in this case is the temporary directory
	$docplate->addVariable("CURDIR",$tempDir);

	# Add pre and post process templates from docplates
	$docplate->addPreProcessTemplate("bits/start.tt2");
	$docplate->addPostProcessTemplate("bits/end.tt2");

	# Process template
	if (!$docplate->process($templateName,{'DOC_ID' => $vars->{'DocumentID'}}, $texFile)) {
		setError($docplate->error);
		return;
	}

	# Create a pdf latex object to handle outputting the PDF
	my $pdflatex = AWIT::PDFLatex->new();
	$pdflatex->setOutputDirectory($tempDir);
	if (my $output = $pdflatex->process($texFile)) {
		setError($output);
		return;
	}

	# Copy PDF to output file	
	if (!copy($pdfFile,$outputFile)) {
		setError("Failed to copy '$pdfFile' to '$outputFile': $!");
		return;
	}

	return $outputFile;
}



# Create a template object
# Args: template_name params
sub loadTemplate
{
	my ($templateName,$params,$output) = @_;


	# Build config
	my $cfg = {
		# Interpolate variables in-place
		INTERPOLATE => 1,
		# Include paths, separated by :
		INCLUDE_PATH => $config->{'path'},
		# Header
		PRE_PROCESS => $config->{'global_header'},
		# Footer
		POST_PROCESS => $config->{'global_footer'},
		# Debug
		DEBUG => $config->{'debug'},
	};

	# Create template object
	my $template = Template->new($cfg);
	if (!$template) {
		setError($Template::ERROR);
		return undef;
	}

	# Process
	my $res = $template->process($templateName,$params,$output);
	if (!$res) {
		setError($template->error());
		return undef;
	}

	return $output;
}


# Internal function to generate a template error
# Args: template_name params
sub abort
{
	die Template::Exception->new("abort",join(': ',@_));
}



1;
# vim: ts=4
