# Client functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::Clients;

use strict;
use warnings;


use wiaflos::constants;
use awitpt::db::dblayer;
use awitpt::cache;
use wiaflos::server::core::GL;


# Whole money transactions, precision is two
use Math::BigFloat;
Math::BigFloat::precision(-2);


# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}


# Check if client ID exists
sub clientIDExists
{
	my $clientID = shift;


	# Select client count
	my $rows = DBSelectNumResults("FROM clients WHERE ID = ".DBQuote($clientID));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}


# Check if client code exists
sub clientCodeExists
{
	my $clientCode = shift;


	# Uppercase and add remove client identifier
	$clientCode = uc($clientCode);
	$clientCode =~ s#^C-##;

	# Select client count
	my $rows = DBSelectNumResults("FROM clients WHERE Code = ".DBQuote($clientCode));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}


# Return client ID from code
sub getClientIDFromCode
{
	my $clientCode = shift;



	# Uppercase and add remove client identifier
	$clientCode = uc($clientCode);
	$clientCode =~ s#^C-##;

	# Check cache
	my ($cache_res,$cache) = cacheGetKeyPair('Clients/Code-to-ID',$clientCode);
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}
	return $cache if (defined($cache));


	# Select client
	my $sth = DBSelect("
		SELECT
			ID
		FROM
			clients
		WHERE
			Code = ".DBQuote($clientCode)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding client '$clientCode'");
		return ERR_NOTFOUND;
	}

	# Cache this
	$cache_res = cacheStoreKeyPair('Clients/Code-to-ID',$clientCode,$row->{'ID'});
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}

	return $row->{'ID'};
}


# Check if transaction number exists
sub transactionNumberExists
{
	my $number = shift;


	# Sanitize
	$number = uc($number);
	$number =~ s#^TRN/##;

	# Select transaction count
	my $rows = DBSelectNumResults("FROM client_account_transactions WHERE Number = ".DBQuote($number));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}


# Return transaction ID from number
sub getTransactionIDFromNumber
{
	my $number = shift;


	# Sanitize
	$number = uc($number);
	$number =~ s#^TRN/##;

	# Check cache
	my ($cache_res,$cache) = cacheGetKeyPair('ClientAccountTransaction/Number-to-ID',$number);
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}
	return $cache if (defined($cache));

	# Select transaction
	my $sth = DBSelect("
		SELECT
			ID
		FROM
			client_account_transactions
		WHERE
			Number = ".DBQuote($number)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding transaction '$number'");
		return ERR_NOTFOUND;
	}

	# Cache this
	$cache_res = cacheStoreKeyPair('ClientAccountTransaction/Number-to-ID',$number,$row->{'ID'});
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}

	return $row->{'ID'};
}



# Backend function to build item hash
sub sanitizeRawItem
{
	my $rawData = shift;


	my $item;

	$item->{'ID'} = $rawData->{'ID'};

	$item->{'Code'} = "C-".uc($rawData->{'Code'});
	$item->{'Name'} = $rawData->{'Name'};
	$item->{'RegNumber'} = $rawData->{'RegNumber'};
	$item->{'TaxReference'} = $rawData->{'TaxReference'};
	$item->{'GLAccountID'} = $rawData->{'GLAccountID'};
	$item->{'GLAccountNumber'} = wiaflos::server::core::GL::getGLAccountNumberFromID($rawData->{'GLAccountID'});
	$item->{'ContactPerson'} = $rawData->{'ContactPerson'};

	return $item;
}


# Backend function to build item hash
sub sanitizeRawAccountTransactionItem
{
	my $rawData = shift;


	my $item;

	$item->{'ID'} = $rawData->{'ID'};

	$item->{'ClientID'} = $rawData->{'ClientID'};

	$item->{'Number'} = "TRN/".uc($rawData->{'Number'});

	$item->{'Reference'} = $rawData->{'Reference'};

	$item->{'GLAccountID'} = $rawData->{'GLAccountID'};

	$item->{'TransactionDate'} = $rawData->{'TransactionDate'};
	$item->{'Amount'} = $rawData->{'Amount'};

	$item->{'GLTransactionID'} = $rawData->{'GLTransactionID'};
	$item->{'Posted'} = defined($rawData->{'GLTransactionID'}) ? 1 : 0;

	$item->{'Closed'} = $rawData->{'Closed'};

	return $item;
}



# Return a hash containing the client
# Optional:
#		ID			- Client id
#		Code		- Client code
sub getClient
{
	my ($detail) = @_;


	my $clientID;

	# Check which 'mode' we operating in
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		# Verify client code
		if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
			setError("No (or invalid) client code provided");
			return ERR_PARAM;
		}

		# Check if client exists
		if (($clientID = getClientIDFromCode($detail->{'Code'})) < 1) {
			setError(Error());
			return $clientID;
		}
	} else {
		$clientID = $detail->{'ID'};
	}

	# Verify client ID
	if (!$clientID || $clientID < 1) {
		setError("No (or invalid) client code/id provided");
		return ERR_PARAM;
	}


	# Return list of clients
	my $sth = DBSelect("
		SELECT
			ID, Code, Name, RegNumber, TaxReference, GLAccountID, ContactPerson
		FROM
			clients
		WHERE
			clients.ID = ".DBQuote($clientID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Code Name RegNumber TaxReference GLAccountID ContactPerson ));

	DBFreeRes($sth);

	return sanitizeRawItem($row);
}


# Return an array of clients
sub getClients
{
	my @clients = ();


	# Return list of clients
	my $sth = DBSelect("
		SELECT
			ID, Code, Name, RegNumber, TaxReference, GLAccountID, ContactPerson
		FROM
			clients
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Code Name RegNumber TaxReference GLAccountID ContactPerson ))) {
		my $item = sanitizeRawItem($row);

		push(@clients,$item);
	}

	DBFreeRes($sth);

	return \@clients;
}


# Create client
# Parameters:
#		Code	- Client code
#		Name	- Client name
#		GLAccountNumber	- General ledger account
# Optional:
#		RegNumber - Company registration number / ID number of person
#		TaxReference - Company tax reference number
#		CreateSubAccount - Create sub GL account for this client
sub createClient
{
	my ($detail) = @_;


	my @extraCols = ();
	my @extraData = ();


	# Verify client code
	if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
		setError("No client code provided");
		return ERR_PARAM;
	}
	# Uppercase and add remove client identifier
	(my $clientCode = uc($detail->{'Code'})) =~ s#^C-##;

	# Verify Name
	if (!defined($detail->{'Name'}) || $detail->{'Name'} eq "") {
		setError("No name provided for client '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Verify GL account
	if (!defined($detail->{'GLAccountNumber'}) || $detail->{'GLAccountNumber'} eq "") {
		setError("No GL account provided for client '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Get GL account ID
	my $GLAccID;
	if (($GLAccID = wiaflos::server::core::GL::getGLAccountIDFromNumber($detail->{'GLAccountNumber'})) < 1) {
		setError(wiaflos::server::core::GL::Error());
		return $GLAccID;
	}

	# Check we an asset account
	my $catMatch = wiaflos::server::core::GL::checkGLAccountFinCat($GLAccID,"A01");
	if ($catMatch == 0) {
		setError("GL account is not of type asset for client '".$detail->{'Code'}."'");
		return ERR_USAGE;
	} elsif ($catMatch < 0) {
		setError(wiaflos::server::core::GL::Error());
		return $catMatch;
	}

	# Check for conflicts
	if ((my $res = clientCodeExists($clientCode)) != 0) {
		# If it exists, err
		if ($res == 1) {
			setError("Client code '".$clientCode."' already exists");
			return ERR_CONFLICT;
		} else {
			setError(Error());
		}

		# else err with the code we got
		return $res;
	}

	# See if we got optional params
	if (defined($detail->{'RegNumber'})) {
		push(@extraCols,'RegNumber');
		push(@extraData,DBQuote($detail->{'RegNumber'}));
	}
	if (defined($detail->{'TaxReference'})) {
		push(@extraCols,'TaxReference');
		push(@extraData,DBQuote($detail->{'TaxReference'}));
	}


	# Pull in extra data
	my $extraCols = "";
	my $extraData = "";
	if (@extraCols > 0 && @extraData > 0) {
		$extraCols .= ',' . join(',',@extraCols);
		$extraData .= ',' . join(',',@extraData);
	}

	DBBegin();

	# Check if we should create a sub account
	if (defined($detail->{'CreateSubAccount'}) && $detail->{'CreateSubAccount'} eq 'y') {

		# Get GL account ID
		my $tmp;
		$tmp->{'AccountNumber'} = $detail->{'GLAccountNumber'};
		my $subGLAcc = wiaflos::server::core::GL::getGLAccount($tmp);
		if (ref $subGLAcc ne "HASH") {
			setError(wiaflos::server::core::GL::Error());
			return $subGLAcc;
		}

		# Create new account
		my $newAcc;
		$newAcc->{'Code'} = wiaflos::server::core::GL::getNextGLSubAccountCode($tmp);
		$newAcc->{'Name'} = "Client: $clientCode";
		$newAcc->{'FinCatCode'} = $subGLAcc->{'FinCatCode'};
		$newAcc->{'RwCatCode'} = $subGLAcc->{'RwCatCode'};
		$newAcc->{'ParentAccountNumber'} = $detail->{'GLAccountNumber'};
		$GLAccID = wiaflos::server::core::GL::createGLAccount($newAcc);
		# Check for error
		if ($GLAccID < 1) {
			setError(wiaflos::server::core::GL::Error());
			return $GLAccID;
		}
	}

	# Create client
	my $sth = DBDo("
		INSERT INTO clients
				(Code,Name,GLAccountID$extraCols)
			VALUES
				(
					".DBQuote($clientCode).",
					".DBQuote($detail->{'Name'}).",
					".DBQuote($GLAccID)."
					$extraData
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("clients","ID");

	DBCommit();

	return $ID;
}


# Link Address
# Parameters:
#		Code		- Client code
#		Type		- Address type, billing or shipping
#		Address		- Address
sub linkClientAddress
{
	my ($detail) = @_;


	# Verify client code
	if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
		setError("No (or invalid) client code provided");
		return ERR_PARAM;
	}

	# Verify type
	if (!defined($detail->{'Type'}) || $detail->{'Type'} eq "") {
		setError("No address type provided for client '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Verify address
	if (!defined($detail->{'Address'}) || $detail->{'Address'} eq "") {
		setError("No address provided for client '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Decide on address type
	my $typeID;
	if ($detail->{'Type'} eq "billing") {
		$typeID = 1;
	} elsif ($detail->{'Type'} eq "shipping") {
		$typeID = 2;
	} else {
		setError("Invalid address type provided for client '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Check if client exists
	my $clientID;
	if (($clientID = getClientIDFromCode($detail->{'Code'})) < 1) {
		setError(Error());
		return $clientID;
	}

	# Link in client address
	my $sth = DBDo("
		INSERT INTO client_addresses
				(ClientID,Type,Address)
			VALUES
				(
					".DBQuote($clientID).",
					".DBQuote($typeID).",
					".DBQuote($detail->{'Address'})."
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("client_addresses","ID");

	return $ID;
}


# Link phone number
# Parameters:
#		Code		- Client code
#		Type		- Number type, phone or fax
#		Number		- Phone number
# Optional:
#		Name		- Name of person/org
sub linkClientPhoneNumber
{
	my ($detail) = @_;


	my @extraCols = ();
	my @extraData = ();


	# Verify client code
	if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
		setError("No (or invalid) client code provided");
		return ERR_PARAM;
	}

	# Verify type
	if (!defined($detail->{'Type'}) || $detail->{'Type'} eq "") {
		setError("No phone number type provided for client '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Verify phone number
	if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
		setError("No phone number provided for client '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Decide on phone number type
	my $typeID;
	if ($detail->{'Type'} eq "phone") {
		$typeID = 1;
	} elsif ($detail->{'Type'} eq "fax") {
		$typeID = 2;
	} else {
		setError("Invalid phone number type provided for client '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Check if client exists
	my $clientID;
	if (($clientID = getClientIDFromCode($detail->{'Code'})) < 1) {
		setError(Error());
		return $clientID;
	}

	if (defined($detail->{'Name'}) && $detail->{'Name'} ne "") {
		push(@extraCols,"Name");
		push(@extraData,DBQuote($detail->{'Name'}));
	}


	# Pull in extra data
	my $extraCols = "";
	my $extraData = "";
	if (@extraCols > 0 && @extraData > 0) {
		$extraCols .= ',' . join(',',@extraCols);
		$extraData .= ',' . join(',',@extraData);
	}

	# Link in client address
	my $sth = DBDo("
		INSERT INTO client_phone_numbers
				(ClientID,Type,Number$extraCols)
			VALUES
				(
					".DBQuote($clientID).",
					".DBQuote($typeID).",
					".DBQuote($detail->{'Number'})."
					$extraData
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("client_phone_numbers","ID");

	return $ID;
}


# Link email address
# Parameters:
#		Code		- Client code
#		Type		- Address type, accounts or ?
#		Address		- Email address
sub linkClientEmailAddress
{
	my ($detail) = @_;


	# Verify client code
	if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
		setError("No (or invalid) client code provided");
		return ERR_PARAM;
	}

	# Verify type
	if (!defined($detail->{'Type'}) || $detail->{'Type'} eq "") {
		setError("No email address type provided for client '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Verify address
	if (!defined($detail->{'Address'}) || $detail->{'Address'} eq "") {
		setError("No email address provided for client '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Decide on address type
	my $typeID;
	if ($detail->{'Type'} eq "accounts") {
		$typeID = 1;
	} elsif ($detail->{'Type'} eq "general") {
		$typeID = 1;
	} else {
		setError("Invalid email address type provided for client '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Check if client exists
	my $clientID;
	if (($clientID = getClientIDFromCode($detail->{'Code'})) < 1) {
		setError(Error());
		return $clientID;
	}

	# Link in client address
	my $sth = DBDo("
		INSERT INTO client_email_addresses
			(ClientID,Type,Address)
		VALUES
			(
				".DBQuote($clientID).",
				".DBQuote($typeID).",
				".DBQuote($detail->{'Address'})."
			)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("client_email_addresses","ID");

	return $ID;
}


# Return an array of client addresses
# Parameters:
#		Code		- Client code
# Optional:
#		ID			- Client ID
sub getClientAddresses
{
	my ($detail) = @_;

	my @addresses = ();


	my $clientID;

	# Check which 'mode' we operating in
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		# Verify client code
		if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
			setError("No (or invalid) client code provided");
			return ERR_PARAM;
		}

		# Check if client exists
		if (($clientID = getClientIDFromCode($detail->{'Code'})) < 1) {
			setError(Error());
			return $clientID;
		}
	} else {
		$clientID = $detail->{'ID'};
	}

	# Return list of addresses
	my $sth = DBSelect("
		SELECT
			ID, ClientID, Type, Address
		FROM
			client_addresses
		WHERE
			ClientID = ".DBQuote($clientID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID ClientID Type Address ))) {
		my $item;

		$item->{'ID'} = $row->{'ID'};

		# Resolve type
		if ($row->{'Type'} == 1) {
			$item->{'Type'} = "billing";
		} elsif ($row->{'Type'} == 2) {
			$item->{'Type'} = "shipping";
		} else {
			$item->{'Type'} = "unknown";
		}

		$item->{'Address'} = $row->{'Address'};

		push(@addresses,$item);
	}

	DBFreeRes($sth);

	return \@addresses;
}


# Return an array of client email addresses
# Parameters:
#		Code		- Client code
# Optional:
#		ID			- Client ID
sub getClientEmailAddresses
{
	my ($detail) = @_;

	my @addresses = ();


	my $clientID;

	# Check which 'mode' we operating in
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		# Verify client code
		if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
			setError("No (or invalid) client code provided");
			return ERR_PARAM;
		}

		# Check if client exists
		if (($clientID = getClientIDFromCode($detail->{'Code'})) < 1) {
			setError(Error());
			return $clientID;
		}
	} else {
		$clientID = $detail->{'ID'};
	}

	# Return list of addresses
	my $sth = DBSelect("
		SELECT
			ID, ClientID, Type, Address
		FROM
			client_email_addresses
		WHERE
			ClientID = ".DBQuote($clientID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID ClientID Type Address ))) {
		my $item;

		$item->{'ID'} = $row->{'ID'};

		# Resolve type
		if ($row->{'Type'} == 1) {
			$item->{'Type'} = "accounts";
		} elsif ($row->{'Type'} == 2) {
			$item->{'Type'} = "general";
		} else {
			$item->{'Type'} = "unknown";
		}

		$item->{'Address'} = $row->{'Address'};

		push(@addresses,$item);
	}

	DBFreeRes($sth);

	return \@addresses;
}


# Return an array of client email phone numbers
# Parameters:
#		Code		- Client code
# Optional:
#		ID			- Client ID
sub getClientPhoneNumbers
{
	my ($detail) = @_;

	my @numbers = ();


	my $clientID;

	# Check which 'mode' we operating in
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		# Verify client code
		if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
			setError("No (or invalid) client code provided");
			return ERR_PARAM;
		}

		# Check if client exists
		if (($clientID = getClientIDFromCode($detail->{'Code'})) < 1) {
			setError(Error());
			return $clientID;
		}
	} else {
		$clientID = $detail->{'ID'};
	}

	# Return list of numbers
	my $sth = DBSelect("
		SELECT
			ID, ClientID, Type, Number
		FROM
			client_phone_numbers
		WHERE
			ClientID = ".DBQuote($clientID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID ClientID Type Number ))) {
		my $item;

		$item->{'ID'} = $row->{'ID'};

		# Resolve type
		if ($row->{'Type'} == 1) {
			$item->{'Type'} = "phone";
		} elsif ($row->{'Type'} == 2) {
			$item->{'Type'} = "fax";
		} else {
			$item->{'Type'} = "unknown";
		}

		$item->{'Number'} = $row->{'Number'};

		push(@numbers,$item);
	}

	DBFreeRes($sth);

	return \@numbers;
}


# Remove client
# Parameters:
#		Code	- Client code
sub removeClient
{
	my ($detail) = @_;


	# Verify client code
	if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
		setError("No (or invalid) client code provided");
		return ERR_PARAM;
	}

	# Check if client exists & pull
	my $tmp;
	$tmp->{'Code'} = $detail->{'Code'};
	my $client = getClient($tmp);
	if (ref $client ne "HASH") {
		setError(Error());
		return $client;
	}

	DBBegin();

	# Remove client
	my $sth = DBDo("DELETE FROM client_addresses WHERE ClientID = ".DBQuote($client->{'ID'})."");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	$sth = DBDo("DELETE FROM client_phone_numbers WHERE ClientID = ".DBQuote($client->{'ID'})."");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	$sth = DBDo("DELETE FROM client_email_addresses WHERE ClientID = ".DBQuote($client->{'ID'})."");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	$sth = DBDo("DELETE FROM clients WHERE ID = ".DBQuote($client->{'ID'})."");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return 0;
}



## @fn getGLAccountEntries($detail)
# Function to return a clients GL account entries
#
# @param detail Parameter hash ref
# @li Code Client code
# @li StartDate Optional start date
# @li EndDate Optional end date
# @li BalanceBroughtForward Optional balance brought forward
#
# @returns GL account entries, @see wiaflos::server::core::GL::getGLAccountEntries
sub getGLAccountEntries
{
	my ($detail) = @_;


	# Verify client code
	if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
		setError("No (or invalid) client code provided");
		return ERR_PARAM;
	}

	# Check if client exists & pull
	my $tmp;
	$tmp->{'Code'} = $detail->{'Code'};
	my $client = getClient($tmp);
	if (ref $client ne "HASH") {
		setError(Error());
		return $client;
	}

	# Build our request
	$tmp = undef;
	$tmp->{'AccountID'} = $client->{'GLAccountID'};
	$tmp->{'StartDate'} = $detail->{'StartDate'};
	$tmp->{'EndDate'} = $detail->{'EndDate'};
	$tmp->{'BalanceBroughtForward'} = $detail->{'BalanceBroughtForward'};

	# Get results
	my $res = wiaflos::server::core::GL::getGLAccountEntries($tmp);
	if (ref $res ne "ARRAY") {
		setError(wiaflos::server::core::GL::Error());
	}

	return $res;
}


# @fn createAccountTransaction($data)
# Create client GL account transaction
#
# @param data
# @li Code - Client Code
# @li Number - Transaction number, ie. TRN/xxyyzz
# @li Reference - GL account entry reference
# @li GLAccountNumber - GL account number to post this transaction to
# @li Date -Date of transaction
# @li Amount - Transaction amount
sub createAccountTransaction
{
	my ($data) = @_;


	# Verify receipt number
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		setError("No (or invalid) transaction number provided");
		return ERR_PARAM;
	}
	(my $transactionNumber = uc($data->{'Number'})) =~ s#^TRN/##;

	# Verify client code
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		setError("No (or invalid) client code provided for client transaction");
		return ERR_PARAM;
	}

	# Verify GL account
	if (!defined($data->{'GLAccountNumber'}) || $data->{'GLAccountNumber'} eq "") {
		setError("No (or invalid) GL account provided for client transaction");
		return ERR_PARAM;
	}

	# Verify date
	if (!defined($data->{'Date'}) || $data->{'Date'} eq "") {
		setError("No (or invalid) date provided for client transaction");
		return ERR_PARAM;
	}

	# Verify reference
	if (!defined($data->{'Reference'}) || $data->{'Reference'} eq "") {
		setError("No (or invalid) reference provided for client transaction");
		return ERR_PARAM;
	}

	# Amount
	if (!defined($data->{'Amount'}) || $data->{'Amount'} eq "") {
		setError("No (or invalid) amount account provided for client transaction");
		return ERR_PARAM;
	}

	# Check if client exists
	my $clientID  = wiaflos::server::core::Clients::getClientIDFromCode($data->{'Code'});
	if ($clientID < 1) {
		setError(wiaflos::server::core::Clients::Error());
		return $clientID;
	}

	# Check GL account exists
	my $GLAccountID = wiaflos::server::core::GL::getGLAccountIDFromNumber($data->{'GLAccountNumber'});
	if ($GLAccountID < 1) {
		setError(wiaflos::server::core::GL::Error());
		return $GLAccountID;
	}

	# Check for conflicts
	if (my $res = transactionNumberExists($transactionNumber)) {
		setError("Transaction number '$transactionNumber' already exists");
		return ERR_CONFLICT;
	}

	# Create transaction
	my $sth = DBDo("
		INSERT INTO client_account_transactions
				(ClientID,Number,Reference,GLAccountID,TransactionDate,Amount,Closed)
			VALUES
				(
					".DBQuote($clientID).",
					".DBQuote($transactionNumber).",
					".DBQuote($data->{'Reference'}).",
					".DBQuote($GLAccountID).",
					".DBQuote($data->{'Date'}).",
					".DBQuote($data->{'Amount'}).",
					0
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("client_account_transactions","ID");

	return $ID;
}


# @fn getAccountTransactions($data)
# Return an array of account transactions
#
# @param data Hash with following elements
# @li Code - Client code
# @li Type - Optional type, 'open' or 'all'
#
# @return Array ref of hash refs, @see sanitizeRawAccountTransactionItem
sub getAccountTransactions
{
	my ($data) = @_;

	# Verify client code
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		setError("No (or invalid) client code provided");
		return ERR_PARAM;
	}

	my $type = defined($data->{'Type'}) ? $data->{'Type'} : "open";

	# Check if client exists & pull
	my $tmp;
	$tmp->{'Code'} = $data->{'Code'};
	my $client = getClient($tmp);
	if (ref $client ne "HASH") {
		setError(Error());
		return $client;
	}

	# Return list of transactions
	my $sth = DBSelect("
		SELECT
			ID, ClientID, Number, Reference, GLAccountID, TransactionDate, Amount, GLTransactionID, Closed
		FROM
			client_account_transactions
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	my @transactions = ();
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID ClientID Number Reference GLAccountID TransactionDate Amount GLTransactionID Closed )
	)) {
		# Check what kind of transactions do we want
		if (($type eq "open") && $row->{'Closed'} eq "0") {
			push(@transactions,sanitizeRawAccountTransactionItem($row));
		} elsif ($type eq "all") {
			push(@transactions,sanitizeRawAccountTransactionItem($row));
		}
	}

	DBFreeRes($sth);

	return \@transactions;
}



# @fn getAccountTransaction($data)
# Return an array of account transactions
#
# @param data Hash with following elements
# @li ID - Transaction ID
# @li Number - Transaction number
#
# @return Hash ref, @see sanitizeRawAccountTransactionItem
sub getAccountTransaction
{
	my ($data) = @_;


	my $transactionID;

	# Check which 'mode' we operating in
	if (!defined($data->{'ID'}) || $data->{'ID'} < 1) {
		# Verify transaction number
		if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
			setError("No (or invalid) transaction number provided");
			return ERR_PARAM;
		}

		# Check if transaction exists
		if (($transactionID = getTransactionIDFromNumber($data->{'Number'})) < 1) {
			setError(Error());
			return $transactionID;
		}
	} else {
		$transactionID = $data->{'ID'};
	}

	# Verify transaction ID
	if (!$transactionID || $transactionID < 1) {
		setError("No (or invalid) transaction number/id provided");
		return ERR_PARAM;
	}

	# Return list of transactions
	my $sth = DBSelect("
		SELECT
			ID, ClientID, Number, Reference, GLAccountID, TransactionDate, Amount, GLTransactionID,	Closed
		FROM
			client_account_transactions
		WHERE
			ID = ".DBQuote($transactionID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch row
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID ClientID Number Reference GLAccountID TransactionDate Amount GLTransactionID	Closed )
	);

	DBFreeRes($sth);

	return sanitizeRawAccountTransactionItem($row);
}


# @fn postAccountTransaction($data)
# Post an account transaction
#
# @param data Hash with the below elements...
# @li Number - Transaction number
#
# @return 0 on success, -1 on error
sub postAccountTransaction
{
	my ($data) = @_;


	my $tmp;

	# Grab transaction
	$tmp = undef;
	$tmp->{'Number'} = $data->{'Number'};
	my $transaction = getAccountTransaction($tmp);
	if (ref $transaction ne "HASH") {
		setError(Error());
		return $transaction;
	}

	# Make sure transaction is not posted
	if ($transaction->{'Posted'} eq "1") {
		setError("Account transaction '".$transaction->{'ID'}."' already posted");
		return ERR_POSTED;
	}

	# Pull in client
	$tmp = undef;
	$tmp->{'ID'} = $transaction->{'ClientID'};
	my $client = wiaflos::server::core::Clients::getClient($tmp);
	if (ref $client ne "HASH") {
		setError(wiaflos::server::core::Clients::Error());
		return $client;
	}

	DBBegin();

	# Create transaction
	$tmp = undef;
	$tmp->{'Date'} = $transaction->{'TransactionDate'};
	$tmp->{'Reference'} = sprintf("Transaction: %s (%s)",$transaction->{'Number'},$transaction->{'Reference'});
	my $GLTransactionID = wiaflos::server::core::GL::createGLTransaction($tmp);
	if ($GLTransactionID < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $GLTransactionID;
	}

	# Pull in amount
	my $transValue = Math::BigFloat->new($transaction->{'Amount'});

	# Link from GL
	$tmp = undef;
	$tmp->{'ID'} = $GLTransactionID;
	$tmp->{'GLAccountID'} = $transaction->{'GLAccountID'};
	$tmp->{'Amount'} = $transValue->bstr();
	if ((my $res = wiaflos::server::core::GL::linkGLTransaction($tmp)) < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	# Negate for other side
	$transValue->bmul(-1);

	# Link to client GL account
	$tmp = undef;
	$tmp->{'ID'} = $GLTransactionID;
	$tmp->{'GLAccountID'} = $client->{'GLAccountID'};
	$tmp->{'Amount'} = $transValue->bstr();
	if ((my $res = wiaflos::server::core::GL::linkGLTransaction($tmp)) < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	# Post transaction
	my $sth = DBDo("
		UPDATE
			client_account_transactions
		SET
			GLTransactionID = ".DBQuote($GLTransactionID)."
		WHERE
			ID = ".DBQuote($transaction->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	# Post transaction
	$tmp = undef;
	$tmp->{'ID'} = $GLTransactionID;
	if ((my $res = wiaflos::server::core::GL::postGLTransaction($tmp)) != 0) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	DBCommit();

	return RES_OK;
}


# @fn linkAccountTransactionAllocation($data)
# Link an allocation to the account transaction
#
# @param data Hash with the below elements...
# @li ID - Account transaction ID
# @li Number - Account transaction number
# @li Amount - Allocation amount
# @li CreditNoteID - Credit note ID to link to
# @li ReceiptAllocationID - Receipt allocation ID to link to
sub linkAccountTransactionAllocation
{
	my ($data) = @_;


	my @extraCols = ();
	my @extraData = ();


	# Grab transaction
	my $tmp;
	$tmp->{'ID'} = $data->{'ID'};
	$tmp->{'Number'} = $data->{'Number'};
	my $transaction = getAccountTransaction($tmp);
	if (ref $transaction ne "HASH") {
		setError(Error());
		return $transaction;
	}

	# Grab transactions
	$tmp = undef;
	$tmp->{'ID'} = $transaction->{'ID'};
	my $transactions = getAccountTransactionAllocations($tmp);
	if (ref $transactions ne "ARRAY") {
		setError(Error());
		return $transactions;
	}

	# Check the transaction total vs. the transaction transactions
	my $transactionBalance = Math::BigFloat->new($transaction->{'Amount'});
	foreach my $item (@{$transactions}) {
		$transactionBalance->badd($item->{'Amount'});
	}

	# And the current transaction
	$transactionBalance->badd($data->{'Amount'});


	DBBegin();

	# If transaction balances out, transaction is now closed
	if ($transaction->{'Closed'} eq "0" && ($transactionBalance->is_zero() || $transactionBalance->is_neg())) {
		my $sth = DBDo("
			UPDATE
				client_account_transactions
			SET
				Closed = 1
			WHERE
				ID = ".DBQuote($transaction->{'ID'})."
		");
		if (!$sth) {
			setError(awitpt::db::dblayer::Error());
			DBRollback();
			return ERR_DB;
		}
	}

	# Check if this is a credit note
	if ($data->{'CreditNoteID'}) {
		push(@extraCols,'CreditNoteID');
		push(@extraData,DBQuote($data->{'CreditNoteID'}));

	# Its a receipt allocation
	} elsif ($data->{'ReceiptAllocationID'}) {
		push(@extraCols,'ReceiptAllocationID');
		push(@extraData,DBQuote($data->{'ReceiptAllocationID'}));
	}

	# Pull in extra tmp
	my $extraCols = "";
	my $extraData = "";
	if (@extraCols > 0 && @extraData > 0) {
		$extraCols .= ',' . join(',',@extraCols);
		$extraData .= ',' . join(',',@extraData);
	}

	# Add transaction allocation
	my $sth = DBDo("
		INSERT INTO client_account_transaction_allocations
				(AccountTransactionID,Amount$extraCols)
			VALUES
				(
					".DBQuote($transaction->{'ID'}).",
					".DBQuote($data->{'Amount'})."
					$extraData
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("client_account_transaction_allocations","ID");

	DBCommit();

	return $ID;
}


# @fn getAccountTransactionAllocations($data)
# Return an array of account transaction allocations
#
# @param data Hash with the below elements...
# @li ID - Account transaction ID
# @li Number - Account transaction number
#
# @return Array ref of hash refs
# @li ID - Account transaction ID
# @li Amount - Allocation amount
# @li CreditNoteID - Credit note ID to link to
# @li ReceiptAllocationID - Receipt allocation ID to link to
sub getAccountTransactionAllocations
{
	my ($detail) = @_;


	# Grab transaction
	my $data;
	$data->{'ID'} = $detail->{'ID'};
	$data->{'Number'} = $detail->{'Number'};
	my $transaction = getAccountTransaction($data);
	if (ref $transaction ne "HASH") {
		setError(Error());
		return $transaction;
	}

	# Return list of account transaction allocations
	my $sth = DBSelect("
		SELECT
			ID, Amount, ReceiptAllocationID
		FROM
			client_account_transaction_allocations
		WHERE
			AccountTransactionID = ".DBQuote($transaction->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	my @allocations = ();
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Amount ReceiptAllocationID ))) {
		my $item;

		$item->{'ID'} = $row->{'ID'};

		$item->{'Amount'} = $row->{'Amount'};

		# If its a receipt, pull in all the details
		if (defined($row->{'ReceiptAllocationID'})) {
			$item->{'ReceiptAllocationID'} = $row->{'ReceiptAllocationID'};

		# If its a credit note pull in all the details for that
		} elsif (defined($row->{'CreditNoteID'})) {
			$item->{'CreditNoteID'} = $row->{'CreditNoteID'};
		}


		push(@allocations,$item);
	}

	DBFreeRes($sth);

	return \@allocations;
}









1;
# vim: ts=4
