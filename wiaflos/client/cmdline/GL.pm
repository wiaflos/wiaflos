# General ledger functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2006-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::cmdline::GL;

use strict;
use warnings;

use wiaflos::constants;
use wiaflos::client::misc;
use wiaflos::client::soap;
use wiaflos::client::reportwriter;

use Math::BigFloat;
Math::BigFloat::precision(-2);


# Plugin info
our $pluginInfo = {
	Name 	=> "GL",
	Menu 	=> [
		# GL main menu option
		{
			MenuItem 	=> "GL",
			Children 	=> [
				{
					MenuItem 	=> "List",
					Regex		=> "list",
					Desc		=> "Display list of accounts",
					Help		=> 'list [format="<text or html>"]',
					Function	=> \&list,
				},
				{
					MenuItem 	=> "Show",
					Regex		=> "show",
					Desc		=> "Show an account",
					Help		=> 'show account="<account number>" [start="<start date>"] [end="<end date>"] [balance-brought-forward="<y|n>"] [type="transaction type bitmask"]',
					Function	=> \&show,
				},
				{
					MenuItem 	=> "Create",
					Regex		=> "create",
					Desc		=> "Create account",
					Help		=> 'create code="<account code>" name="<account name>" fincat="<financial category code>" rwcat="<reporting category code>" [parent="<parent account number>"]',
					Function	=> \&createAccount,
				},

				{
					MenuItem 	=> "Transactions",
					Regex		=> "trans(?:actions)?",
					Children	=> [
						{
							MenuItem 	=> "Create",
							Regex		=> "create",
							Desc		=> "Create transaction",
							Help		=> 'create date="<YYYY-MM-DD>" reference="<reference>" type="<type>"',
							Function	=> \&createTransaction,
						},
						{
							MenuItem 	=> "List",
							Regex		=> "list",
							Desc		=> "List transactions",
							Help		=> 'list [start="<start date>"] [end="<end date>"] [type="<type>"]',
							Function	=> \&listTransactions,
						},
						{
							MenuItem 	=> "Link",
							Regex		=> "link",
							Desc		=> "Link transactions",
							Help		=> 'link transaction="<transaction id>" account="<account number>" <amount|credit|debit>="<amount>" [reference="<reference>"]',
							Function	=> \&linkTransaction,
						},
						{
							MenuItem 	=> "Post",
							Regex		=> "post",
							Desc		=> "post transactions",
							Help		=> 'post transaction="<transaction id>"',
							Function	=> \&postTransaction,
						},
						{
							MenuItem 	=> "Remove",
							Regex		=> "remove",
							Desc		=> "Remove transactions",
							Help		=> 'remove transaction="<transaction id>"',
							Function	=> \&removeTransaction,
						},
						{
							MenuItem 	=> "Show",
							Regex		=> "show",
							Desc		=> "Display transaction entries",
							Help		=> 'show transaction="<transaction id>"',
							Function	=> \&showTransaction,
						},
					],
				},
			],
		},
	],
};



# Storage of last ID's, very usefull
my %last;




# GL list of accounts
sub list
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	my $res = soapCall($OUT,"wiaflos/server/api/GL","getGLAccounts");

	if ($res->{'Result'} == RES_OK) {
		if (!defined($parms->{'format'}) || $parms->{'format'} eq "text") {
			print $OUT swrite(<<'END', "ID", "Number", "Name", "FinCat", "RwCat");
+===========+===================+==================================================================================+================+================+
| @|||||||| | @|||||||||||||||| | @||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| | @||||||||||||| | @||||||||||||| |
+===========+===================+==================================================================================+================+================+
END
		} elsif (defined($parms->{'format'}) && $parms->{'format'} eq "html") {
			print($OUT "<table>\n");
			print($OUT "<tr><td>ID</td><td>Number</td><td>Name</td><td>FinCat</td></tr>\n");
		}

		# Sort accounts
		my @accounts = sort {uc($a->{'Number'}) cmp uc($b->{'Number'})} @{$res->{'Data'}};

		foreach my $account (@accounts) {
			if (!defined($parms->{'format'}) || $parms->{'format'} eq "text") {
				print $OUT swrite(<<'END', $account->{'ID'}, $account->{'Number'}, $account->{'Name'}, $account->{'FinCatCode'} . "/" . $account->{'FinCatDescription'},  $account->{'RwCatCode'} . "/" . $account->{'RwCatDescription'});
| @<<<<<<<< | @<<<<<<<<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @<<<<<<<<<<<<< | @<<<<<<<<<<<<< |
END
			} elsif (defined($parms->{'format'}) && $parms->{'format'} eq "html") {
				printf($OUT '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>'."\n",$account->{'ID'}, $account->{'Number'}, $account->{'Name'}, $account->{'FinCatCode'} . "/" . $account->{'FinCatDescription'}, $account->{'RwCatCode'} . "/" . $account->{'RwCatDescription'});
			}
		}
		if (!defined($parms->{'format'}) || $parms->{'format'} eq "text") {
			print $OUT swrite(<<'END');
+===========+===================+==================================================================================+================+================+
END
		} elsif (defined($parms->{'format'}) && $parms->{'format'} eq "html") {
			print($OUT "<table>\n");
		}
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Show GL account entries
sub show
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'account'})) {
		print($OUT "  => ERROR: Parameter 'account' not defined\n");
		return ERR_C_PARAM;
	}
	my $detail;
	$detail->{'AccountNumber'} = $parms->{'account'};
	$detail->{'StartDate'} = $parms->{'start'};
	$detail->{'EndDate'} = $parms->{'end'};
	$detail->{'BalanceBroughtForward'} = $parms->{'balance-brought-forward'};
	$detail->{'Type'} = $parms->{'type'};
	my $res = soapCall($OUT,"wiaflos/server/api/GL","getGLAccountEntries",$detail);

	if ($res->{'Result'} == RES_OK) {
		print $OUT swrite(<<'END', "ID", "Date", "Ref", "Type", "Amount", "Balance");
+===========+============+================================================================================+=========+==============+================+
| @|||||||| | @||||||||| | @||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| | @|||||| | @||||||||||| | @||||||||||||| |
+===========+============+================================================================================+=========+==============+================+
END
		# Sort data
		my @sorted = sort {$a->{'TransactionDate'} cmp $b->{'TransactionDate'}} @{$res->{'Data'}};

		my $balance = Math::BigFloat->new(0);

		foreach my $entry (@sorted) {
			$balance->badd($entry->{'Amount'});
			print $OUT swrite(<<'END', $entry->{'TransactionID'}, $entry->{'TransactionDate'}, $entry->{'Reference'} ? $entry->{'Reference'} : $entry->{'TransactionReference'}, $entry->{'TransactionType'}, sprintf('%8.2f',$entry->{'Amount'}),sprintf('%8.2f',$balance));
| @<<<<<<<< | @<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @|||||| | @>>>>>>>>>>> | @>>>>>>>>>>>>> |
END
		}
		print $OUT swrite(<<'END');
+===========+============+================================================================================+=========+==============+================+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Create transaction
sub createTransaction
{
	my ($OUT,@args) = @_;

	my $parms = parseArgs(@args);

	if (!defined($parms->{'date'})) {
		print($OUT "  => ERROR: Parameter 'date' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'reference'})) {
		print($OUT "  => ERROR: Parameter 'reference' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Date'} = $parms->{'date'};
	$detail->{'Reference'} = $parms->{'reference'};
	$detail->{'Type'} = $parms->{'type'};
	my $res = soapCall($OUT,"wiaflos/server/api/GL","createGLTransaction",$detail);
	if ($res->{'Result'} == RES_OK) {
		# Save this ID
		$last{'transactionid'} = $res->{'Data'};
	 } else {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# postTransaction
sub postTransaction
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'transaction'})) {
		print($OUT "  => ERROR: Parameter 'transaction' not defined\n");
		return ERR_C_PARAM;
	}

	# Set last if we must
	$parms->{'transaction'} = $last{'transactionid'} if ($parms->{'transaction'} eq "last");

	my $detail;
	$detail->{'ID'} = $parms->{'transaction'};
	my $res = soapCall($OUT,"wiaflos/server/api/GL","postGLTransaction",$detail);
	if ($res->{'Result'} == RES_OK) {
		# Null the id
		$last{'transactionid'} = undef;
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Remove transaction
sub removeTransaction
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'transaction'})) {
		print($OUT "  => ERROR: Parameter 'transaction' not defined\n");
		return ERR_C_PARAM;
	}

	# Set last if we must
	$parms->{'transaction'} = $last{'transactionid'} if ($parms->{'transaction'} eq "last");

	my $detail;
	$detail->{'ID'} = $parms->{'transaction'};
	my $res = soapCall($OUT,"wiaflos/server/api/GL","removeGLTransaction",$detail);

	if ($res->{'Result'} == RES_OK) {
		# Null the id
		$last{'transactionid'} = undef;
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# showTransaction
sub showTransaction
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'transaction'})) {
		print($OUT "  => ERROR: Parameter 'transaction' not defined\n");
		return ERR_C_PARAM;
	}

	# Set last if we must
	$parms->{'transaction'} = $last{'transactionid'} if ($parms->{'transaction'} eq "last");

	my $detail;
	$detail->{'ID'} = $parms->{'transaction'};
	my $res = soapCall($OUT,"wiaflos/server/api/GL","getGLTransactionEntries",$detail);

	if ($res->{'Result'} == RES_OK) {
		print $OUT swrite(<<'END', "ID", "GL Account", "Ref", "Amount");
+===========+===================+========================================================================================+================+
| @|||||||| | @|||||||||||||||| | @||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| | @||||||||||||| |
+===========+===================+========================================================================================+================+
END
		my $balance = Math::BigFloat->new(0);
		foreach my $entry (@{$res->{'Data'}}) {
			print $OUT swrite(<<'END', $entry->{'ID'}, $entry->{'GLAccountNumber'}, $entry->{'Reference'} ? $entry->{'Reference'} : $entry->{'TransactionReference'}, sprintf('%8.2f',$entry->{'Amount'}));
| @<<<<<<<< | @<<<<<<<<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>>>>>> |
END
			$balance->badd($entry->{'Amount'});
		}
		print swrite(<<'END', $balance->bstr());
+===========+===================+========================================================================================+================+
|                                                                                                    Transaction Balance | @>>>>>>>>>>>>> |
+========================================================================================================================+================+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Restul'};
}


# Link transaction
sub linkTransaction
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'transaction'})) {
		print($OUT "  => ERROR: Parameter 'transaction' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'account'})) {
		print($OUT "  => ERROR: Parameter 'account' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'amount'}) && !defined($parms->{'credit'}) && !defined($parms->{'debit'})) {
		print($OUT "  => ERROR: Parameter 'amount' not defined\n");
		return ERR_C_PARAM;
	}

	# Set last if we must
	$parms->{'transaction'} = $last{'transactionid'} if ($parms->{'transaction'} eq "last");

	my $detail;
	$detail->{'ID'} = $parms->{'transaction'};
	$detail->{'GLAccountNumber'} = $parms->{'account'};
	$detail->{'Amount'} = $parms->{'amount'};
	$detail->{'Credit'} = $parms->{'credit'};
	$detail->{'Debit'} = $parms->{'debit'};
	$detail->{'Reference'} = $parms->{'reference'};
	my $res = soapCall($OUT,"wiaflos/server/api/GL","linkGLTransaction",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# List transactions
sub listTransactions
{
	my ($OUT,@args) = @_;

	my $parms = parseArgs(@args);

	my $detail;
	$detail->{'StartDate'} = $parms->{'start'};
	$detail->{'EndDate'} = $parms->{'end'};
	$detail->{'Type'} = $parms->{'type'};
	my $res = soapCall($OUT,"wiaflos/server/api/GL","getGLTransactions",$detail);

	if ($res->{'Result'} == RES_OK) {
		print $OUT swrite(<<'END', "ID", "TransDate", "Ref", "Type", "Posted");
+===========+============+===============================================================================+========+========+
| @|||||||| | @||||||||| | @|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| | @||||| | @||||| |
+===========+============+===============================================================================+========+========+
END
		foreach my $tran (@{$res->{'Data'}}) {
			print $OUT swrite(<<'END', $tran->{'ID'}, $tran->{'TransactionDate'}, $tran->{'Reference'}, $tran->{'Type'}, $tran->{'Posted'});
| @<<<<<<<< | @<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @||||| | @<<<<< |
END
			}
			print $OUT swrite(<<'END');
+===========+============+===============================================================================+========+========+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Create account
sub createAccount
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'code'})) {
		print($OUT "  => ERROR: Parameter 'code' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'name'})) {
		print($OUT "  => ERROR: Parameter 'name' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'fincat'})) {
		print($OUT "  => ERROR: Parameter 'fincat' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'rwcat'})) {
		print($OUT "  => ERROR: Parameter 'rwcat' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Code'} = $parms->{'code'};
	$detail->{'Name'} = $parms->{'name'};
	$detail->{'FinCatCode'} = $parms->{'fincat'};
	$detail->{'RwCatCode'} = $parms->{'rwcat'};
	$detail->{'ParentAccountNumber'} = $parms->{'parent'};

	my $res = soapCall($OUT,"wiaflos/server/api/GL","createGLAccount",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}





1;
# vim: ts=4
