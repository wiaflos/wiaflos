# Engine functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::cmdline::Engine;

use strict;
use warnings;

use wiaflos::constants;
use wiaflos::client::misc;
use wiaflos::client::soap;
use wiaflos::client::reportwriter;


# Plugin info
our $pluginInfo = {
	Name 	=> "Engine",
	Menu 	=> [
		# Engine main menu option
		{
			MenuItem 	=> "Engine",
			Children 	=> [
				{
					MenuItem 	=> "CacheStats",
					Regex		=> "cachestats",
					Desc		=> "Display cache stats",
					Help		=> 'cacheStats',
					Function	=> \&cacheGetStats,
				},
			],
		},
	],
};




# Display cache stats
sub cacheGetStats
{
	my ($OUT,@args) = @_;


	if (@args > 0) {
		print($OUT "  => ERROR: Too many arguments provided\n");
		return ERR_C_PARAM;
	}

	my $detail;
	my $res = soapCall($OUT,"wiaflos/server/api/Engine","cacheGetStats",$detail);

	if ($res->{'Result'} == RES_OK) {
		print swrite(<<'END', "Hits", "Misses");
+=======================+=========================+
| @|||||||||||||||||||| | @|||||||||||||||||||||| |
+=======================+=========================+
END
		print swrite(<<'END', $res->{'Data'}->{'Hits'}, $res->{'Data'}->{'Misses'});
| @>>>>>>>>>>>>>>>>>>>> | @>>>>>>>>>>>>>>>>>>>>>> |
END
		print swrite(<<'END');
+=======================+=========================+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}



1;
# vim: ts=4
