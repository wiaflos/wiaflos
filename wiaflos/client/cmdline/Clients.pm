# Clients functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::cmdline::Clients;

use strict;
use warnings;

use wiaflos::constants;
use wiaflos::client::misc;
use wiaflos::client::soap;
use wiaflos::client::reportwriter;


# Plugin info
our $pluginInfo = {
	Name 	=> "Clients",
	Menu 	=> [
		# Clients main menu option
		{
			MenuItem 	=> "Clients",
			Children 	=> [
				{
					MenuItem 	=> "Create",
					Regex		=> 'create',
					Desc		=> "Create a client",
					Help		=> 'create code="<reference>" name="<client name>" [reg="<registration num>"] [taxref="<tax reference number>"] account="<GL account>" [contactperson="<contact person name>"] [createsubaccount="<y to create sub account>"]',
					Function	=> \&create,
				},
				{
					MenuItem 	=> "LinkAddress",
					Regex		=> "linkaddr(?:ess)?",
					Desc		=> "Link in an address",
					Help		=> 'linkAddress client="<client code>" type="<billing or shipping>" address="<address>"',
					Function	=> \&linkAddress,
				},
				{
					MenuItem 	=> "LinkEmailAddress",
					Regex		=> "linkemail(?:address)?",
					Desc		=> "Link in an email address",
					Help		=> 'linkEmailAddress client="<client code>" type="<accounts or general>" address="<address>"',
					Function	=> \&linkEmailAddress,
				},
				{
					MenuItem 	=> "LinkPhoneNumber",
					Regex		=> "link(?:phnum|phonenumber)",
					Desc		=> "Link in a phone number",
					Help		=> 'linkPhoneNumber client="<client code>" type="<phone or fax>" number="<phone number>" [name="<name>"]',
					Function	=> \&linkPhoneNumber,
				},
				{
					MenuItem 	=> "Remove",
					Regex		=> "remove",
					Desc		=> "Remove client",
					Help		=> 'remove client="<client code>"',
					Function	=> \&remove,
				},
				{
					MenuItem 	=> "List",
					Regex		=> "list",
					Desc		=> "Display list of clients",
					Help		=> 'list',
					Function	=> \&list,
				},
				{
					MenuItem 	=> "Show",
					Regex		=> "show",
					Desc		=> "Show client",
					Help		=> 'show client="<client code>"',
					Function	=> \&show,
				},
				{
					MenuItem 	=> "ShowGL",
					Regex		=> "showgl",
					Desc		=> "Show GL account entries",
					Help		=> 'showGL client="<client code>" [start="<start date>"] [end="<end date>"] [balance-brought-forward="<y|n>"]',
					Function	=> \&showGLAccountEntries,
				},
				{
					MenuItem 	=> "CreateTransaction",
					Regex		=> "createtransaction",
					Desc		=> "Create client GL account transaction",
					Help		=> 'createTransaction client="<client code>" number="<transaction number>" reference="<transaction reference>" account="<GL account>" date="<transaction date>" amount="<amount>"',
					Function	=> \&createTransaction,
				},
				{
					MenuItem 	=> "PostTransaction",
					Regex		=> "postTransaction",
					Desc		=> "Post client GL transaction",
					Help		=> 'postTransaction transaction="<transaction number>"',
					Function	=> \&postTransaction,
				},
				{
					MenuItem 	=> "ShowTransactions",
					Regex		=> "showTransactions",
					Desc		=> "Show client GL account transactions",
					Help		=> 'showTransactions client="<client code>" [type="all"|"open"]',
					Function	=> \&showTransactions,
				},
				{
					MenuItem 	=> "ShowTransactionAllocations",
					Regex		=> "showTransactionAllocations",
					Desc		=> "Show client GL account transaction allocations",
					Help		=> 'showTransactionAllocations transaction="<transaction code>"',
					Function	=> \&showTransactionAllocations,
				},
			],
		},
	],
};


# List clients
sub list
{
	my ($OUT,@args) = @_;

	if (@args > 0) {
		print($OUT "  => ERROR: Too many arguments provided\n");
		return ERR_C_PARAM;
	}

	my $res = soapCall($OUT,"wiaflos/server/api/Clients","getClients");

	if ($res->{'Result'} == RES_OK) {
		print $OUT swrite(<<'END', "ID", "Code", "Name", "RegNumber", "TaxRef", "GLAcc");
+========+===========+===============================================+=====================+================+=================+
| @||||| | @|||||||| | @|||||||||||||||||||||||||||||||||||||||||||| | @|||||||||||||||||| | @||||||||||||| | @|||||||||||||| |
+========+===========+===============================================+=====================+================+=================+
END
		foreach my $item (@{$res->{'Data'}}) {
			print $OUT swrite(<<'END', $item->{'ID'}, $item->{'Code'}, $item->{'Name'}, defined($item->{'RegNumber'}) ? $item->{'RegNumber'} : '-', defined($item->{'TaxReference'}) ? $item->{'TaxReference'} : '-', $item->{'GLAccountNumber'});
| @<<<<< | @<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @<<<<<<<<<<<<<<<<<< | @<<<<<<<<<<<<< | @<<<<<<<<<<<<<< |
END
			}
		print $OUT swrite(<<'END');
+========+===========+===============================================+=====================+================+=================+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Create client
sub create
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'code'})) {
		print($OUT "  => ERROR: Parameter 'code' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'name'})) {
		print($OUT "  => ERROR: Parameter 'name' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'account'})) {
		print($OUT "  => ERROR: Parameter 'account' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Code'} = $parms->{'code'};
	$detail->{'Name'} = $parms->{'name'};
	$detail->{'GLAccountNumber'} = $parms->{'account'};
	$detail->{'RegNumber'} = $parms->{'reg'};
	$detail->{'TaxReference'} = $parms->{'taxref'};
	$detail->{'CreateSubAccount'} = $parms->{'createsubaccount'};
	my $res = soapCall($OUT,"wiaflos/server/api/Clients","createClient",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Link address
sub linkAddress
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'client'})) {
		print($OUT "  => ERROR: Parameter 'client' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'type'})) {
		print($OUT "  => ERROR: Parameter 'type' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'address'})) {
		print($OUT "  => ERROR: Parameter 'address' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Code'} = $parms->{'client'};
	$detail->{'Type'} = $parms->{'type'};
	$detail->{'Address'} = $parms->{'address'};
	my $res = soapCall($OUT,"wiaflos/server/api/Clients","linkClientAddress",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Link email address
sub linkEmailAddress
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'client'})) {
		print($OUT "  => ERROR: Parameter 'client' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'type'})) {
		print($OUT "  => ERROR: Parameter 'type' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'address'})) {
		print($OUT "  => ERROR: Parameter 'address' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Code'} = $parms->{'client'};
	$detail->{'Type'} = $parms->{'type'};
	$detail->{'Address'} = $parms->{'address'};
	my $res = soapCall($OUT,"wiaflos/server/api/Clients","linkClientEmailAddress",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Link phone number
sub linkPhoneNumber
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'client'})) {
		print($OUT "  => ERROR: Parameter 'client' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'type'})) {
		print($OUT "  => ERROR: Parameter 'type' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'number'})) {
		print($OUT "  => ERROR: Parameter 'number' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Code'} = $parms->{'client'};
	$detail->{'Type'} = $parms->{'type'};
	$detail->{'Number'} = $parms->{'number'};
	$detail->{'Name'} = $parms->{'name'};
	my $res = soapCall($OUT,"wiaflos/server/api/Clients","linkClientPhoneNumber",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Show client
sub show
{
	my ($OUT,@args) = @_;

	my $parms = parseArgs(@args);

	if (!defined($parms->{'client'})) {
		print($OUT "  => ERROR: Parameter 'client' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Code'} = $parms->{'client'};

	my $res;

	$res = soapCall($OUT,"wiaflos/server/api/Clients","getClient",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $client = $res->{'Data'};

	$res = soapCall($OUT,"wiaflos/server/api/Clients","getClientAddresses",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $addies = $res->{'Data'};

	$res = soapCall($OUT,"wiaflos/server/api/Clients","getClientPhoneNumbers",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $phones = $res->{'Data'};

	$res = soapCall($OUT,"wiaflos/server/api/Clients","getClientEmailAddresses",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $emailAddies = $res->{'Data'};

	print($OUT "
ID.....: ".$client->{'ID'}."
Ref....: ".$client->{'Code'}."
Name...: ".$client->{'Name'}."
Reg....: ".(defined($client->{'RegNumber'}) ? $client->{'RegNumber'} : "-")."
TaxRef.: ".(defined($client->{'TaxReference'}) ? $client->{'TaxReference'} : "-")."
GLAcc..: ".$client->{'GLAccountNumber'}."

");

	foreach my $item (@{$addies}) {
		print($OUT "
Address: ".$item->{'Type'}."
".$item->{'Address'}."
");
	}

	foreach my $item (@{$phones}) {
		print($OUT "
Phone Number: ".$item->{'Type'}."
".$item->{'Number'}."
");
	}

	foreach my $item (@{$emailAddies}) {
		print($OUT "
Email Address: ".$item->{'Type'}."
".$item->{'Address'}."
");
	}

	return RES_OK;
}


# Remove client
sub remove
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'client'})) {
		print($OUT "  => ERROR: Parameter 'client' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Code'} = $parms->{'client'};
	my $res= soapCall($OUT,"wiaflos/server/api/Clients","removeClient",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Show supplier GL account entries
sub showGLAccountEntries
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'client'})) {
		print($OUT "  => ERROR: Parameter 'client' not defined\n");
		return ERR_C_PARAM;
	}
	my $detail;
	$detail->{'Code'} = $parms->{'client'};
	$detail->{'StartDate'} = $parms->{'start'};
	$detail->{'EndDate'} = $parms->{'end'};
	$detail->{'BalanceBroughtForward'} = $parms->{'balance-brought-forward'};
	my $res = soapCall($OUT,"wiaflos/server/api/Clients","getClientGLAccountEntries",$detail);

	if ($res->{'Result'} == RES_OK) {
		print $OUT swrite(<<'END', "ID", "Date", "Ref", "Amount", "Balance");
+===========+============+================================================================================+==============+================+
| @|||||||| | @||||||||| | @||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| | @||||||||||| | @||||||||||||| |
+===========+============+================================================================================+==============+================+
END
		# Sort data
		my @sorted = sort {$a->{'TransactionDate'} cmp $b->{'TransactionDate'}} @{$res->{'Data'}};

		my $balance = Math::BigFloat->new(0);

		foreach my $entry (@sorted) {
			$balance->badd($entry->{'Amount'});
			print $OUT swrite(<<'END', $entry->{'ID'}, $entry->{'TransactionDate'}, $entry->{'Reference'} ? $entry->{'Reference'} : $entry->{'TransactionReference'}, sprintf('%8.2f',$entry->{'Amount'}),sprintf('%8.2f',$balance));
| @<<<<<<<< | @<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>>>> | @>>>>>>>>>>>>> |
END
		}
		print $OUT swrite(<<'END');
+===========+============+================================================================================+==============+================+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# @li Code - Client Code
# @li Number - Transaction number, ie. TRN/xxyyzz
# @li Reference - GL account entry reference
# @li GLAccountNumber - GL account number to post this transaction to
# @li Date -Date of transaction
# @li Amount - Transaction amount
# Create client
sub createTransaction
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'client'})) {
		print($OUT "  => ERROR: Parameter 'client' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'number'})) {
		print($OUT "  => ERROR: Parameter 'number' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'reference'})) {
		print($OUT "  => ERROR: Parameter 'reference' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'account'})) {
		print($OUT "  => ERROR: Parameter 'account' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'date'})) {
		print($OUT "  => ERROR: Parameter 'date' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'amount'})) {
		print($OUT "  => ERROR: Parameter 'amount' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Code'} = $parms->{'client'};
	$detail->{'Number'} = $parms->{'number'};
	$detail->{'Reference'} = $parms->{'reference'};
	$detail->{'GLAccountNumber'} = $parms->{'account'};
	$detail->{'Date'} = $parms->{'date'};
	$detail->{'Amount'} = $parms->{'amount'};
	my $res = soapCall($OUT,"wiaflos/server/api/Clients","createAccountTransaction",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Show acocunt transactions
sub showTransactions
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'client'})) {
		print($OUT "  => ERROR: Parameter 'client' not defined\n");
		return ERR_C_PARAM;
	}
	my $detail;
	$detail->{'Code'} = $parms->{'client'};
	$detail->{'Type'} = $parms->{'type'};
	my $res = soapCall($OUT,"wiaflos/server/api/Clients","getAccountTransactions",$detail);

	if ($res->{'Result'} == RES_OK) {
		print $OUT swrite(<<'END', "ID", "Number", "Date", "Reference", "Amount","Posted","Closed");
+===========+===================+=============+=========================================+==============+========+========+
| @|||||||| | @|||||||||||||||| | @|||||||||| | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>>>> | @||||| | @||||| |
+===========+===================+=============+=========================================+==============+========+========+
END
		# Sort data
		my @sorted = sort {$a->{'TransactionDate'} cmp $b->{'TransactionDate'}} @{$res->{'Data'}};

		my $balance = Math::BigFloat->new(0);

		foreach my $entry (@sorted) {
			$balance->badd($entry->{'Amount'});
			print $OUT swrite(<<'END', $entry->{'ID'}, $entry->{'Number'}, $entry->{'TransactionDate'}, $entry->{'Reference'}, sprintf('%8.2f',$entry->{'Amount'}),$entry->{'Posted'},$entry->{'Closed'});
| @<<<<<<<< | @<<<<<<<<<<<<<<<< | @<<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @<<<<<<<<<<< | @||||| | @||||| |
END
		}
		print $OUT swrite(<<'END');
+===========+===================+=============+=========================================+==============+========+========+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Post transaction
sub postTransaction
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'transaction'})) {
		print($OUT "  => ERROR: Parameter 'transaction' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Number'} = $parms->{'transaction'};
	my $res = soapCall($OUT,"wiaflos/server/api/Clients","postAccountTransaction",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Show transaction allocations
sub showTransactionAllocations
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'transaction'})) {
		print($OUT "  => ERROR: Parameter 'client' not defined\n");
		return ERR_C_PARAM;
	}
	my $detail;
	$detail->{'Number'} = $parms->{'transaction'};
	my $res = soapCall($OUT,"wiaflos/server/api/Clients","getAccountTransactionAllocations",$detail);

	if ($res->{'Result'} == RES_OK) {
		print $OUT swrite(<<'END', "ID", "Amount", "CreditNoteID", "ReceiptAllocationID");
+===========+===============+=============+============+
| @|||||||| | @|||||||||||| | @|||||||||| | @||||||||| |
+===========+===============+=============+============+
END
		# Sort data
		my $balance = Math::BigFloat->new(0);
		foreach my $entry (@{$res->{'Data'}}) {
			$balance->badd($entry->{'Amount'});
			print $OUT swrite(<<'END', $entry->{'ID'}, sprintf('%.2f',$entry->{'Amount'}), "", $entry->{'ReceiptAllocationID'});
| @|||||||| | @>>>>>>>>>>>> | @|||||||||| | @||||||||| |
+===========+===============+=============+============+
END
		}
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}






1;
# vim: ts=4
