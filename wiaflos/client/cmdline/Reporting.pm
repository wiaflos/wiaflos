# Reporting functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::cmdline::Reporting;

use strict;
use warnings;


use wiaflos::constants;
use wiaflos::client::misc;
use wiaflos::client::soap;
use wiaflos::client::reportwriter;

use Math::BigFloat;
Math::BigFloat::precision(-2);


# Plugin info
our $pluginInfo = {
	Name 	=> "Reporting",
	Menu 	=> [
		# Reporting Menu
		{
			MenuItem 	=> "Reporting",
			Regex		=> "report(?:ing)?",
			Children	=> [
				{
					MenuItem 	=> "ShowChartOfAccounts",
					Regex		=> "show(?:chartofaccounts|coa)",
					Desc		=> "Display chart of accounts",
					Help		=> 'showChartOfAccounts [start=<yyyy-mm-dd>] [end=<yyyy-mm-dd>] [levels=<levels to display>]',
					Function	=> \&show,
				},
				{
					MenuItem 	=> "Send",
					Regex		=> "send",
					Desc		=> "Send report",
					Help		=> 'send template="<template>" start="<yyyy-mm-dd>" [account="<gl_account>"] [subject="<subject>"] [background="<1|y|yes>"] sendto="<email[:addy1,addy2...] or file:filename>"',
					Function	=> \&send,
				},
			],
		},
	],
};



# Show report
sub show
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	my $detail;
	$detail->{'StartDate'} = $parms->{'start'};
	$detail->{'EndDate'} = $parms->{'end'};
	$detail->{'Levels'} = $parms->{'levels'};
	my $res = soapCall($OUT,"wiaflos/server/api/Reporting","getChartOfAccounts",$detail);
	if ($res->{'Result'} == RES_OK) {
		print swrite(<<'END', "Number", "Name", "Debit", "Credit");
+===================+==================================================================================+================+================+
| @|||||||||||||||| | @||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| | @||||||||||||| | @||||||||||||| |
+===================+==================================================================================+================+================+
END
		my $debit = Math::BigFloat->new(0);
		my $credit = Math::BigFloat->new(0);

		# Sort data
		my %byRwCat;
		my %rwCatDesc;
		foreach my $entry (@{$res->{'Data'}}) {
			push(@{$byRwCat{$entry->{'RwCatCode'}}},$entry) if ($entry->{'Level'} == 0);
			$rwCatDesc{$entry->{'RwCatCode'}} = $entry->{'RwCatDescription'};
		}

		print swrite(<<'END',"Assets");
| @||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| |
END
		foreach my $entry (@{$byRwCat{'00'}}) {
			$debit->badd($entry->{'DebitAmount'});
			$credit->badd($entry->{'CreditAmount'});

			# Resolve children up to 'levels' print blank amounts for parents

			print swrite(<<'END', $entry->{'Number'}, $entry->{'Name'}, defined($entry->{'DebitAmount'}) ? sprintf('%.2f',$entry->{'DebitAmount'}) : '', defined($entry->{'CreditAmount'}) ? sprintf('%.2f',$entry->{'CreditAmount'}) : '');
| @<<<<<<<<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>>>>>> | @>>>>>>>>>>>>> |
END
		}





		print swrite(<<'END',"Liabilities");
| @||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| |
END
		foreach my $entry (@{$byRwCat{'20'}}) {
			$debit->badd($entry->{'DebitAmount'});
			$credit->badd($entry->{'CreditAmount'});
			print swrite(<<'END', $entry->{'Number'}, $entry->{'Name'}, defined($entry->{'DebitAmount'}) ? sprintf('%.2f',$entry->{'DebitAmount'}) : '', defined($entry->{'CreditAmount'}) ? sprintf('%.2f',$entry->{'CreditAmount'}) : '');
| @<<<<<<<<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>>>>>> | @>>>>>>>>>>>>> |
END
		}

		print swrite(<<'END',"Equity");
| @||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| |
END
		foreach my $entry (@{$byRwCat{'40'}}) {
			$debit->badd($entry->{'DebitAmount'});
			$credit->badd($entry->{'CreditAmount'});
			print swrite(<<'END', $entry->{'Number'}, $entry->{'Name'}, defined($entry->{'DebitAmount'}) ? sprintf('%.2f',$entry->{'DebitAmount'}) : '', defined($entry->{'CreditAmount'}) ? sprintf('%.2f',$entry->{'CreditAmount'}) : '');
| @<<<<<<<<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>>>>>> | @>>>>>>>>>>>>> |
END
		}

		print swrite(<<'END',"Income");
| @||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| |
END
		foreach my $entry (@{$byRwCat{'60'}}) {
			$debit->badd($entry->{'DebitAmount'});
			$credit->badd($entry->{'CreditAmount'});
			print swrite(<<'END', $entry->{'Number'}, $entry->{'Name'}, defined($entry->{'DebitAmount'}) ? sprintf('%.2f',$entry->{'DebitAmount'}) : '', defined($entry->{'CreditAmount'}) ? sprintf('%.2f',$entry->{'CreditAmount'}) : '');
| @<<<<<<<<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>>>>>> | @>>>>>>>>>>>>> |
END
		}

		print swrite(<<'END',"Expenses");
| @||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| |
END
		foreach my $entry (@{$byRwCat{'80'}}) {
			$debit->badd($entry->{'DebitAmount'});
			$credit->badd($entry->{'CreditAmount'});
			print swrite(<<'END', $entry->{'Number'}, $entry->{'Name'}, defined($entry->{'DebitAmount'}) ? sprintf('%.2f',$entry->{'DebitAmount'}) : '', defined($entry->{'CreditAmount'}) ? sprintf('%.2f',$entry->{'CreditAmount'}) : '');
| @<<<<<<<<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>>>>>> | @>>>>>>>>>>>>> |
END
		}



		print swrite(<<'END',$debit->bstr(),$credit->bstr());
+======================================================================================================+================+================+
|                                                                                                      | @||||||||||||| | @||||||||||||| |
+======================================================================================================+================+================+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Send report
sub send
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'template'})) {
		print($OUT "  => ERROR: Parameter 'template' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'sendto'})) {
		print($OUT "  => ERROR: Parameter 'sendto' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Template'} = $parms->{'template'};
	$detail->{'StartDate'} = $parms->{'start'};
	$detail->{'EndDate'} = $parms->{'end'};
	$detail->{'SendTo'} = $parms->{'sendto'};
	$detail->{'GLAccountNumber'} = $parms->{'account'};
	$detail->{'Subject'} = $parms->{'subject'};
	$detail->{'Background'} = $parms->{'background'};
	my $res = soapCall($OUT,"wiaflos/server/api/Reporting","sendReport",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}



1;
# vim: ts=4
