# Payments functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::cmdline::Payments;

use strict;
use warnings;



use wiaflos::constants;
use wiaflos::client::misc;
use wiaflos::client::soap;
use wiaflos::client::reportwriter;

use Math::BigFloat;
Math::BigFloat::precision(-2);


# Plugin info
our $pluginInfo = {
	Name 	=> "Payments",
	Menu 	=> [
		# Suppliers main menu option
		{
			MenuItem 	=> "Suppliers",
			Children 	=> [
				# Payments
				{
					MenuItem 	=> "Payments",
					Regex		=> "pay(?:ments?)?",
					Children => [
						{
							MenuItem 	=> "CreatePayment",
							Regex		=> "create",
							Desc		=> "Create a payment",
							Help		=> 'create supplier="<supplier code>" account="<GL account source of payment>" number="<payment number>" date="<payment date>" reference="<journal reference>" amount="<amount paid>"',
							Function	=> \&createPayment,
						},
						{
							MenuItem 	=> "List",
							Regex		=> "list",
							Desc		=> "List payments",
							Help		=> 'list [type="<all|open>"]',
							Function	=> \&list,
						},
						{
							MenuItem 	=> "Post",
							Regex		=> "post",
							Desc		=> "Post payment",
							Help		=> 'post payment="<payment number>"',
							Function	=> \&postPayment,
						},
						{
							MenuItem 	=> "Allocate",
							Regex		=> "alloc(?:ate)?",
							Desc		=> "Allocate monies from a payment",
							Help		=> 'allocate payment="<payment number>" invoice="<supplier invoice number>" amount="<amount to allocate>"',
							Function	=> \&createAllocation,
						},
						{
							MenuItem 	=> "PostAllocation",
							Regex		=> "postalloc(?:ation)?",
							Desc		=> "Post payment allocation",
							Help		=> 'postAllocation id="<allocation ID>>"',
							Function	=> \&postAllocation,
						},
						{
							MenuItem 	=> "Show",
							Regex		=> "show",
							Desc		=> "Show payment allocations",
							Help		=> 'show payment="<payment number>"',
							Function	=> \&show,
						},
					],
				},
			],
		},
	],
};



# Last ID info
my %last;


# Create payment
sub createPayment
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'supplier'})) {
		print($OUT "  => ERROR: Parameter 'supplier' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'account'})) {
		print($OUT "  => ERROR: Parameter 'account' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'number'})) {
		print($OUT "  => ERROR: Parameter 'number' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'date'})) {
		print($OUT "  => ERROR: Parameter 'date' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'reference'})) {
		print($OUT "  => ERROR: Parameter 'reference' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'amount'})) {
		print($OUT "  => ERROR: Parameter 'amount' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'SupplierCode'} = $parms->{'supplier'};
	$detail->{'GLAccountNumber'} = $parms->{'account'};
	$detail->{'Number'} = $parms->{'number'};
	$detail->{'Date'} = $parms->{'date'};
	$detail->{'Reference'} = $parms->{'reference'};
	$detail->{'Amount'} = $parms->{'amount'};
	my $res = soapCall($OUT,"wiaflos/server/api/Payments","createPayment",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# List payments
sub list
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	# build request
	my $detail;
	$detail->{'Type'} = $parms->{'type'};
	my $res = soapCall($OUT,"wiaflos/server/api/Payments","getPayments",$detail);

	if ($res->{'Result'} == RES_OK) {
			print swrite(<<'END', "ID", "Supplier", "GLAccount", "Payment", "TransDate", "Ref", "Amount");
Payment list
+=======+===========+==============+============================+=============+==============================================+============+
| @|||| | @|||||||| | @||||||||||| | @||||||||||||||||||||||||| | @|||||||||| | @||||||||||||||||||||||||||||||||||||||||||| | @||||||||| |
+=======+===========+==============+============================+=============+==============================================+============+
END

		foreach my $payment (@{$res->{'Data'}}) {
				print swrite(<<'END', $payment->{'ID'}, $payment->{'SupplierCode'}, $payment->{'GLAccountNumber'}, $payment->{'Number'}, $payment->{'TransactionDate'}, $payment->{'Reference'}, sprintf('%.2f',$payment->{'Amount'}));
| @<<<<<< | @<<<<<< | @<<<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<< | @<<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>> |
END
		}
			print swrite(<<'END');
+=======+===========+==============+============================+=============+==============================================+============+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Post payment
sub postPayment
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'payment'})) {
		print($OUT "  => ERROR: Parameter 'payment' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Number'} = $parms->{'payment'};
	my $res = soapCall($OUT,"wiaflos/server/api/Payments","postPayment",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Create allocation
sub createAllocation
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'payment'})) {
		print($OUT "  => ERROR: Parameter 'payment' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'invoice'})) {
		print($OUT "  => ERROR: Parameter 'invoice' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'amount'})) {
		print($OUT "  => ERROR: Parameter 'amount' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'PaymentNumber'} = $parms->{'payment'};
	$detail->{'SupplierInvoiceNumber'} = $parms->{'invoice'};
	$detail->{'Amount'} = $parms->{'amount'};
	my $res = soapCall($OUT,"wiaflos/server/api/Payments","createPaymentAllocation",$detail);
	# Save this ID
	if ($res->{'Result'} == RES_OK) {
		# Save this ID
		$last{'allocationid'} = $res->{'Data'};
	 } else {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Post allocation
sub postAllocation
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'id'})) {
		print($OUT "  => ERROR: Parameter 'id' not defined\n");
		return ERR_C_PARAM;
	}

	# Set last if we must
	$parms->{'id'} = $last{'allocationid'} if ($parms->{'id'} eq "last");

	my $detail;
	$detail->{'ID'} = $parms->{'id'};
	my $res = soapCall($OUT,"wiaflos/server/api/Payments","postPaymentAllocation",$detail);
	if ($res->{'Result'} == RES_OK) {
		# Null the id
		$last{'allocationid'} = undef;
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Show payment allocations
sub show
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'payment'})) {
		print($OUT "  => ERROR: Parameter 'payment' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	my $res;

	$detail->{'Number'} = $parms->{'payment'};
	$res = soapCall($OUT,"wiaflos/server/api/Payments","getPayment",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $payment = $res->{'Data'};

	$detail = undef;
	$detail->{'PaymentNumber'} = $parms->{'payment'};
	$res = soapCall($OUT,"wiaflos/server/api/Payments","getPaymentAllocations",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $allocs = $res->{'Data'};

	print swrite(<<'END', $parms->{'payment'}, "ID", "SupplierInvoice", "Amount", "Posted", "Balance");
Payment allocations for: @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
+=======+===================+==============+========+==============+
| @|||| | @|||||||||||||||| | @||||||||||| | @||||| | @||||||||||| |
+=======+===================+==============+========+==============+
END
	my $balance = Math::BigFloat->new(0);
	my $unposted = Math::BigFloat->new(0);

	foreach my $alloc (@{$allocs}) {
		$balance->badd($alloc->{'Amount'}) if ($alloc->{'Posted'});
		$unposted->badd($alloc->{'Amount'}) if (!$alloc->{'Posted'});
		print swrite(<<'END', $alloc->{'ID'}, $alloc->{'SupplierInvoiceNumber'}, sprintf('%.2f',$alloc->{'Amount'}), $alloc->{'Posted'}, $alloc->{'Posted'} ? $balance->bstr() : '');
| @<<<< | @<<<<<<<<<<<<<<<< | @>>>>>>>>>>> | @||||| | @>>>>>>>>>>> |
END
	}

	my $available = Math::BigFloat->new($payment->{'Amount'});
	$available->precision(-2);
	$available->bsub($balance);

	$balance->bmul(-1);

	print swrite(<<'END',sprintf('%.2f',$payment->{'Amount'}),sprintf('%.2f',$balance->bstr()),sprintf('%.2f',$unposted->bstr()),sprintf('%.2f',$available->bstr()));
+=======+===================+==============+========+==============+
|                                    Payment Amount | @>>>>>>>>>>> |
|                                   Allocated Funds | @>>>>>>>>>>> |
|                                    Unposted Funds | @>>>>>>>>>>> |
+===================================================+==============+
|                                   Available Funds | @>>>>>>>>>>> |
+===================================================+==============+
END

	return RES_OK;
}




1;
# vim: ts=4
