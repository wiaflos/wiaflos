# Tax functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::cmdline::Tax;

use strict;
use warnings;


use wiaflos::constants;
use wiaflos::client::misc;
use wiaflos::client::soap;
use wiaflos::client::reportwriter;



# Plugin info
our $pluginInfo = {
	Name 	=> "Tax",
	Menu 	=> [
		# Tax main menu option
		{
			MenuItem 	=> "Tax",
			Children 	=> [
				{
					MenuItem 	=> "CreateType",
					Regex		=> "createtype",
					Desc		=> "Create a tax type",
					Help		=> 'createType description="<description>" account="<GL account>" rate="<tax rate>"',
					Function	=> \&createType,
				},
			],
		},
	],
};



# Create type
sub createType
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'description'})) {
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'account'})) {
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'rate'})) {
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Description'} = $parms->{'description'};
	$detail->{'GLAccountNumber'} = $parms->{'account'};
	$detail->{'Rate'} = $parms->{'rate'};

	my $res = soapCall($OUT,"wiaflos/server/api/Tax","createTaxType",$detail);

	if ($res->{'Result'} != RES_OK) {
		soapDebug($res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}





1;
# vim: ts=4
