# Constants
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::constants;

use strict;

# Exporter stuff
require Exporter;
our (@ISA,@EXPORT,@EXPORT_OK);
@ISA = qw(Exporter);
@EXPORT = qw(
	ERR_UNKNOWN


	ERR_SRVPARAM
	ERR_SRVFILE
	ERR_SRVEXEC
	ERR_SRVFORK
	ERR_SRVCACHE
	ERR_SRVTEMPLATE


	ERR_DB


	ERR_PARAM
	ERR_S_PARAM
	ERR_C_PARAM

	ERR_USAGE
	ERR_S_USAGE
	ERR_C_USAGE


	ERR_NOTFOUND
	ERR_CONFLICT
	ERR_POSTED
	ERR_PAID


	ERR_DISCNDISC
	ERR_AMTZERO
	ERR_OVERALLOC
	ERR_OVERALLOC_QTY
	ERR_NOBALANCE
	ERR_NODETADDR
	ERR_INVALID_QTY



	RES_OK
	RES_ERROR


	CONFIG_FILE
);
@EXPORT_OK = ();


use constant {
	ERR_UNKNOWN	=>	-1,


	ERR_SRVPARAM	=>	-501, # -8
	ERR_SRVFILE		=>	-511,
	ERR_SRVEXEC		=>	-512,
	ERR_SRVFORK		=>	-513,
	ERR_SRVCACHE	=>	-590,
	ERR_SRVTEMPLATE	=>	-600,


	ERR_DB		=>	-2001, # -2


	ERR_PARAM	=>	-3001, # -3
	ERR_S_PARAM	=>	-103001, # -101
	ERR_C_PARAM	=>	-203001, # -201

	ERR_USAGE	=>	-3002, # -5
	ERR_S_USAGE	=>	-103002, # -5
	ERR_C_USAGE	=>	-203002, # -5


	ERR_NOTFOUND	=>	-4001, # -4
	ERR_CONFLICT	=>	-4010, # -10
	ERR_POSTED	=>	-4101, # -9
	ERR_PAID	=>	-4102,


	ERR_DISCNDISC	=>	-8001, # -6
	ERR_AMTZERO		=>	-8002,
	ERR_OVERALLOC	=>	-8010, # -11
	ERR_OVERALLOC_QTY	=>	-8011,
	ERR_NOBALANCE	=>	-8012,
	ERR_NODETADDR	=>	-8100, # -7
	ERR_INVALID_QTY	=>	-8200,



	RES_OK			=> 0,
	RES_ERROR		=> -1,



	CONFIG_FILE		=> "/etc/wiaflos-server.conf",
};



1;
# vim: ts=4
