[% INCLUDE header.tt2 %]

<head>
	<title>Balance Sheet</title>
	[% INCLUDE styles.tt2 %]
</head>

<body>

[% # Pull account balances from backend
	SET data = api_account_balances(  api_bitwise_and(API_RPT_BALANCE_BF,API_RPT_INCLUDE_YEAREND) )
%]

<table id="page" width="100%">
	<colgroup span="2" width="50%">
	</colgroup>
	[% INCLUDE companydetails.tt2 %]
	<tr>
		<td colspan="2" class="bigtext border centeralign bold">
			B A L A N C E &nbsp;&nbsp;&nbsp; S H E E T
			<br />
			as at [% EndDate %]
		</td>
	</tr>
	<tr>
		<td colspan="3" class="smalltext">
			<table width="100%">
				<tr>
					<td class="border centeralign">Account</td>
					<td class="border centeralign">Description</td>
					<td class="border centeralign">Balance</td>
				</tr>
					[%  # Loop with Assets, Liabilities & Equity
						FOREACH rwcatcode IN [ '00', '20', '40' ] 
					%]

						[%  # Create a float to total up this reporting category
							api_variable_new(rwcatcode,'float') 
						%]

						<tr>
							<td class="centeralign bold" colspan="6">
								[% data.ReportWriterCategories.${rwcatcode}.Code %]
								- 
								[% data.ReportWriterCategories.${rwcatcode}.Description %]
							</td>
						</tr>

						[% # Loop with main accounts in this category
							FOREACH item IN data.ReportWriterCategoryToAccounts.${rwcatcode} 
						%]
							<tr>
								<td class="bold">[% item.Number %]</td>
								<td class="bold">[% item.Name %]</td>
								<td class="rightalign bold">
									[% # For liabilities and equity, reverse amount and use brackets
										IF rwcatcode == '20' || rwcatcode == '40' 
									%]
										[% api_format_amount(item.ClosingBalance,API_FMT_REVERSE + API_FMT_NEGBRACKET) %]
									[% ELSE %]
										[% api_format_amount(item.ClosingBalance,API_FMT_NEGBRACKET) %]
									[% END %]
								</td>
							</tr>

							[% # Add amount to variable
								api_variable_add(rwcatcode,item.ClosingBalance) 
							%]

							[% # If we have children
								IF item.Children 
							%]
								[% # Loop with children 
									FOREACH child IN item.Children 
								%]
									<tr>
										<td>[% child.Number %]</td>
										<td >[% child.Name %]</td>
										<td class="rightalign">
											[% # For liabilities and equity, reverse amount and use brackets
												IF rwcatcode == '20' || rwcatcode == '40' 
											%]
												[% api_format_amount(child.ClosingBalance,API_FMT_REVERSE + API_FMT_NEGBRACKET) %]
											[% ELSE %]
												[% api_format_amount(child.ClosingBalance,API_FMT_NEGBRACKET) %]
											[% END %]
										</td>
									</tr>
								[% END %]
							[% END %]
						[% END %]
						<tr>
							<td style="border-top: solid 2px black; border-bottom: solid 1px black;" class="bold" colspan="2">
								[% data.ReportWriterCategories.${rwcatcode}.Description %] Total
							</td>
							<td style="border-top: solid 2px black; border-bottom: solid 1px black;" class="rightalign bold">
								[% # For liabilities and equity, reverse amount and use brackets
									IF rwcatcode == '20' || rwcatcode == '40' 
								%]
									[% api_format_amount(api_variable_getraw(rwcatcode),API_FMT_REVERSE + API_FMT_NEGBRACKET) %]
								[% ELSE %]
									[% api_format_amount(api_variable_getraw(rwcatcode),API_FMT_NEGBRACKET) %]
								[% END %]
							</td>
						</tr>
					[% END %]
				<tr>
					<td style="border-top: double 2px black;" class="bold" colspan="2">TOTAL LIABILITIES AND EQUITY</td>
					[% api_variable_new('total_liabilities_equity','float') %]
					[% api_variable_add('total_liabilities_equity',api_variable_getraw('20')) %]
					[% api_variable_add('total_liabilities_equity',api_variable_getraw('40')) %]
					<td style="border-top: double 2px black;" class="rightalign bold">
						[% api_format_amount(api_variable_getraw('total_liabilities_equity'),API_FMT_REVERSE + API_FMT_NEGBRACKET) %]
					</td>
				</tr>
				<tr>
					<td class="centeralign" colspan="4">All values in <span class="bold">USD</span> funds.</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<div class="smalltext centeralign">[% WiaflosString %]</div>



</body>

</html>
