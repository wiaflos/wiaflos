<?php
# Online GPG file verification script
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
# Copyright (C) 2008, LinuxRulz
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.







# Check if form was submitted
if (!empty($_FILES)) {
	$sample_file = $_FILES['file_sample']['tmp_name']; 
	$signature_file = $_FILES['file_signature']['tmp_name']; 
}

if (!empty($sample_file) && !empty($signature_file)) {
	$mydir = dirname($_SERVER["SCRIPT_FILENAME"]);

	$res = exec("/usr/bin/gpg \
				--no-options \
				--homedir='$mydir/keyring' \
				--no-auto-check-trustdb \
				--lock-never \
				--no-permission-warning \
				--verify '$signature_file' '$sample_file' 2>&1
		",$output,$ret);

	if ($ret == 0) {
?>
		File '<?php echo $_FILES['file_sample']['name']; ?>': Verificaton successful. This file is authentic and unmodified.
<?php
	} elseif ($ret == 1) {
?>
		File '<?php echo $_FILES['file_sample']['name']; ?>': Verificaton FAILED. This file is not authentic and could of been tampered with. (Code: <?php echo $ret; ?>)
<?php
	} else {
?>
		An error occured. Please try again.
<?php
	}


# Not submitted?
} else {
?>
<form enctype="multipart/form-data" action="gpg.php" method="POST">
<table border="0">
	<tr>
		<td>File to verify:</td>
		<td><input name="file_sample" type="file" /></td>
	</tr>
	<tr>
		<td>Digital signature (.asc):</td>
		<td><input name="file_signature" type="file" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" value="Verify" /></td>
	</tr>
</table>
</form>
<?php
}


?>
