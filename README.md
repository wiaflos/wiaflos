# Wiaflos Accounting

Wiaflos Accounting is an extremely efficient light-weight multi-platform server-client based accounting system.

The goal is to have a central accounting engine which can either run on a dedicated server or on the same PC as the client interface. The accounting engine will provide a full SOAP API for easy writing of GUI/Web interfaces or tying into in-house systems. Seeing as the accounting engine is written in Perl and makes use of a database abstraction layer, the engine should run on nearly any operating system and use nearly any database system supported by Perl and the target OS.

We want this package to be of equal use to the one-man-show as a full accounting package running on a single PC as well as to large multi-national corporate entities with dedicated accounting servers using the package as a fast stable accounting function, solution or full package.

<http://wiki.wiaflos.org/>


## Testing

Instructions below are for testing or installation of a SQLite based system.

1. Install dependencies:

    `sudo apt-get install -y git libcache-fastmmap-perl libconfig-inifiles-perl libcrypt-gpg-perl libdatetime-perl libdbd-sqlite3-perl libmime-lite-perl` libsoap-lite-perl libtemplate-perl sqlite3

2. Pull in sub repositories:

    `./update-git-modules`

3. Configure:

    `sed -e 's/DSN=DBI:mysql:database=wiaflos;host=localhost/DSN=DBI:SQLite:dbname=wiaflos.sqlite/' -i wiaflos-server.conf`

4. Load database:

    ```
    awitpt/bin/convert-tsql sqlite database/schema.tsql > database/schema.sqlite
    sqlite3 wiaflos.sqlite < database/schema.sqlite
    ```

5. Load data:

    `export PERL5LIB="$PWD"; ./wiaflos-admin --config=wiaflos-server.conf --connect="local" --load-file contrib/books/10-basic.wiaflos`

6. Type `help` at anytime to get help, or `help <command>`


## Installation

These instructions are for installation of a MySQL based system.

1. Install dependencies:

    `sudo apt-get install -y git libcache-fastmmap-perl libconfig-inifiles-perl libcrypt-gpg-perl libdatetime-perl libdbd-mysql-perl libmime-lite-perl libsoap-lite-perl libtemplate-perl mysql-server`

2. Pull in sub repositories:

    `./update-git-modules`

3. Create configuration file /etc/wiaflos-server.conf using wiaflos-server.conf as a starting point.

4. Create database wiaflos and initialize:

    ```
    awitpt/bin/convert-tsql mysql database/schema.tsql > database/schema.sql
    mysql -u root wiaflos <  database/schema.sql
    ```

5. Run the admin interface:

    `export PERL5LIB="$PWD"; ./wiaflos-admin --connect="local"`


## Perl Modules

```
    perl-DBD-MySQL or perl-DBD-SQLite
    perl-DBI

    perl-Cache-FastMmap
    perl-Config-IniFiles
    perl-Crypt-GPG
    perl-DateTime
    perl-Net-Server
    perl-SOAP-Lite
    perl-Template-Toolkit
    perl-TimeDate

    perl-Term-ReadLine-Gnu (Optional)
```
